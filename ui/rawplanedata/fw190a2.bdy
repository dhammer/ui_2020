FOCKE WULF FW 190A-2
-= Full weight =-
8770 lb (3978 kg)

-= Best speed and FTH =-
348 mph at sea level (560 kmh)
380 mph at 6000 ft (612 kmh at 1800 m)
418 mph at 19000 ft (673 kmh at 5800 m)

-= Ca time to 16400 ft (5000 m) at Bst1 =-
5.7 min

-= Service Ceiling =-
36000 ft (11000 m)

-= Best turn rate at S.L., 50% fuel and no flaps =-
18.3 sec/lap, 760 ft radius

-= Power ON stall speed at 75% fuel =-
98 mph (158 kmh)

-= Max recommended dive speed (IAS) =-
466 mph (750 kmh)

-= Other notes =-
Superb roll rate. Quite sudden stall.


The Fw 190A-2 was quite similar to the A-1. The A-2 however had the slightly more powerful BMW 801 C-2 engine and had better firepower with two extra 20 mm cannons added, giving the A-2 a powerful armament indeed. The Fw 190 quickly established itself as a serious threath to the Allied fighters. While it was far inferior in a turn fight against a fighter like the Spitfire, it's high top speed meant that the German pilots could boom n zoom at their own terms, with the Allied pilots having a hard time to catch this foe.

After a short association with the Fw-190, many new players imagine this aircraft as a large monstrous brute of a plane. In reality, the Wuerger (Butcherbird) is a compact, elegant, and beautiful fighter.
 
Conceived by Focke Wulf designer Kurt Tank in 1937, the Fw-190 only saw production because it used a BMW air cooled radial engine rather than the inline liquid cooled engines earmarked for the Bf-109. When it was introduced, it was more than a match for the RAF fighters it faced, and it maintained a fearsome reputation throughtout the war. The Focke Wulf fighter saw many variants, and served with fighter/interceptor, dive bombing, recon, and even torpedo groups everywhere the Luftwaffe fought. 
 
The first Fw-190 prototype flew in June 1939 and production versions started rolling off the assembly line in November 1940. Of the versions modeled in the game, The Fw-190A-4 entered service in the Summer of 1942 and gave allied pilots a powerful opponent. The heavily gunned A-8 variant first saw action in Mid 1944 and was produced in the largest numbers of any version, and the Fw-190D-9, a high altitude fast interceptor with the Jumo inline liquid cooled engine, was made only in small numbers late in the war though it is considered the finest prop driven fighter the Germans produced in WW2.
 
Erich Rudorffer scored a record 13 kills in one sortie while flying the Fw-190, a testament to the lethality of the plane.
 
The thing that most gives the Butcherbird its awesome reputation is its armament. Most versions featured 4 20mm cannon along with two 7.9mm machine guns. The Fw-190A-8 is the heaviest armed fighter in the game with two of its four 20mm cannon being high velocity MG-151's, and two 13mm MG's in the cowling.