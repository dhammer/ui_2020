REPUBLIC P-47D-22 "THUNDERBOLT"
-= Full weight =-
13467.5 lb (6109 kg)

-= Best speed and FTH =-
345 mph at sea level (555 kmh)
401 mph at 15000 ft (645 kmh at 4600 m)
438 mph at 25000 ft (705 kmh at 7600 m)

-= Ca time to 16400 ft (5000 m) at Bst1 =-
7.1 min

-= Service Ceiling =-
41000 ft (12500 m)

-= Best turn rate at S.L., 50% fuel and no flaps =-
19.0 sec/lap, 887 ft radius

-= Power ON stall speed at 75% fuel =-
106 mph (171 kmh)

-= Max recommended dive speed (IAS) =-
512 mph (824 kmh)

-= Other notes =-
Excels at high altitudes, should avoid low alt fighting. 10 minutes water injection capacity per sortie.


Big, heavy, fast....and ugly. The P-47 Thunderbolt, or "Jug", was undoubtedly the finest ground attack fighter of WW2 and a tremendous air to air weapon as well. There is nothing elegant or pretty about this aircraft, but it was well loved by the men who flew it for one simple fact: It could absorb brutal punishment and bring its pilot home.
 
In 1941, Alexander Kartveli and the Servesky Aircraft Corporation, later Republic, delivered the first XP-47 prototypes for testing to the USAAC. After initial trials, several changes were made to the original airframe, including the use of the big Pratt/Whitney R-2800 Double Wasp radial engine, resulting in the biggest and heaviest fighter aircraft in the world. At over 6 tons, the design drew criticism from some circles, but the speed, range, and armament of 8 .50 cal MGs were enough to offset doubts. Designed as a high altitude interceptor, the P-47 was extremely fast above 20,000 feet and gained in other performance areas as well over fighters not as well suited to the near stratospheric heights.
 
So fast was the Thunderbolt in a dive that the new phenomenon of compressibility became an issue, controls surfaces locking in the vice-like grip of the air at the 500 mph speeds commonly reached in a dive from altitude. Several pilots were lost before this was solved by adding dive flaps to maintain proper airflow at high speeds.
 
"Jug" pilots used their superior speed and diving capabilities to fight enemy planes that turned and accelerated better, and the P-47 with drop tanks was the best long range bomber escort the US had until the introduction of the P-51 Mustang. With its eight .50's, speed, payload, and ability to withstand damage, the P-47 was also the premier ground attack fighter of its day. As the allies won air-superiority in Europe, Thunderbolts ranged all along the western front attacking German ground forces and speeding their collapse.
 
The two top P-47 aces were Robert Johnson and Francis Gabreski of the 56th FG. They tied each other as the leading US aces in the ETO with 28 victories each. Gabreski added another 6.5 victories to his total while flying the F-86 in Korea.