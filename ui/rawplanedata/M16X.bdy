Sd.Kfz. 7/1 armed with a 2 cm Flakvierling 38 L/65 quadruple anti-aircraft gun mounting, appearing with both open and armored cab. 750-800 produced by the end of December 1944. The Sd.Kfz. 7/1 saw extensive use in the North African Campaign where their tracks allowed them to drive through the desert sands far more effectively than trucks. Often columns carrying troops or POW's would include at least two half tracks with one generally riding point in order to make a path through the sands that the trucks could follow.

Armament:                                           

A prime mover for smaller towed guns, such as the 2 cm FlaK 30 or the 3.7 cm PaK 36 anti-tank gun, capable of carrying up to 8 soldiers in addition.

Ammunition load:

None