NAKAJIMA KI-43-II "HAYABUSA" (OSCAR)
-= Full weight =-
5710 lb (2590 kg)

-= Best speed and FTH =-
295 mph at sea level (475 kmh)
328 mph at 9200 ft (528 kmh at 2804 m)
346 mph at 20300 ft (557 kmh at 6200 m)

-= Ca time to 16400 ft (5000 m) at Bst1 =-
5.4 min

-= Service Ceiling =-
37000 ft (11300 m)

-= Best turn rate at S.L., 50% fuel and no flaps =-
11.6 sec/lap, 385 ft radius

-= Power ON stall speed at 75% fuel =-
71 mph (114 kmh)

-= Max recommended dive speed (IAS) =-
400 mph (644 kmh)

-= Other notes =-
Very similar to an A6M3 in performance. Relatively fragile. No armored windscreen.


In January 1939 the first of three Nakajima Ki-43 prototypes made its initial flight. A replacement for the Ki-27, the production model Ki-43-I Hayabusa, outfitted with an 1100 hp Ha-105 engine, began to arrive in units in 1941.
 
Weakly armed with but two 12.7mm machine guns (rather than the original armament of two 7.7mm MG's) the Ki-43-I was soon replaced in November 1942 by the Ki-43-II which had a bigger engine (the Ha-115), stronger wings with attachment points for two 551 lb bombs or drop tanks, cockpit armor, and rubber encased fuel tanks. Unfortunately, it carried the same pair of 12.7mm MG's.
 
Code named "Oscar" by the allies, The Ki-43 served with the Japanese Army air forces and was one of the most maneuverable fighters of the war. Unfortunately, the emphasis of the day was on speed and effective tactics by its opponents made it inadequate for the most part. Still, Ki-43's remained in front-line service until the end of the war, with 5,919 planes produced.