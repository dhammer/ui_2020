In the late 1930's, development of the jet engine sparked the design of the Me-262 "Sturmvogel" (Stormbird). Created by a group of Messerschmitt engineers led by Woldemar Voigt, the 262 was born of a marriage of pioneering concepts and lucky guesses.

To look at the ME-262 is to be immediately reminded of a shark. The triangle shape of the fuselage, the menacing nose with its four 30mm cannon, the mottled grey camouflage...the plane has the look and feel of nature's swift, powerful predator. 

When it made it's debut as the world's first operational combat jet in 1944, the 262 was still, in the opinion of many of it's pilots and designers, not yet suitable for combat. The Jumo 004 jet engines were underpowered and unreliable. The jet required a long runway for takeoff and landing, and was a long time in accelerating to speed. To maintain its edge over allied fighters, the 262 had to remain fast, so slowing down for an accurate guns pass was not advisable. Also, the 30mm cannon, though powerful, had a slow muzzle velocity, making gunnery even more difficult. This was very much an "expert's plane".
Unfortunately, by the period in which it operated, Luftwaffe experten were in short supply.

Still, there was simply nothing that could touch the ME-262 once the pilot had reached altitude and speed. The jet performed well at high altitudes where other fighters struggled, was at least 150 knots faster than anything it faced, and its cannon were more than enough to bring down the bombers it hunted. All in all, the ME-262 was very much a successful design. 

An experimental jet unit, Ekdo 262, received its first 262's in April 1944. Kommando Nowotny, the first combat squadron to be equipped with the ME-262, was declared operational in October of that year. Such units as JG7 and Galland's JV-44 "Squadron of Experts" operated with the ME-262 until the end of the war, with a total of 1,433 aircraft produced. Variants included nightfighter, divebomber, and recon versions.

The top Me-262 ace was Oberstleutnant Heinz Baer, with 16 kills.

In WarBirds, the 262 will see limited action. It will remain very much an expert's plane, because while hard to kill, it is extremely hard to get kills in it. Its best use will likely be in scenario play intercepting B-17's. In scenarios, where the qualities of speed and big guns are at a premium, the fight to get a ride in the jet will be the worst fight you'll face.

Armament:                     
                                   Ammo Load          
 
Primary  : 2 x 30mm cannon         100 rpg            

Secondary: 2 x 30mm cannon         80 rpg             

All cannon are fuselage mounted. 
 
 
The "Ammo Load" is based on the historic normal operational ammo load for the plane/weapon(s) in question.