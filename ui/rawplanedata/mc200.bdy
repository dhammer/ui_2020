MACCHI C.202 "SAETTA"
-= Full weight =-
5330.8 lb (2418 kg)

-= Best speed and FTH =-
282 mph at sea level (454 kmh)
319 mph at 9800 ft (513 kmh at 3000 m)
311 mph at 16400 ft (501 kmh at 5000 m)

-= Ca time to 16400 ft (5000 m) at Bst1 =-
6.7 min

-= Service Ceiling =-
28000 ft (8500 m)

-= Best turn rate at S.L., 50% fuel and no flaps =-
13.8 sec/lap, 448 ft radius

-= Power ON stall speed at 75% fuel =-
75 mph (121 kmh)

-= Max recommended dive speed (IAS) =-
500 mph (805 kmh)

-= Other notes =-
Good controls at all speeds. Reduced torque effects due to designed difference between the left vs right wing span.


The Macchi Castoldi 200 Saetta (Arrow) was designed by Mario Castoldi, Aeronautica Macchi's lead designer, to serve as a modern monoplane fighter aircraft, furnished with retractable landing gear and powered by the Fiat A.74 R.C.38 radial engine capable of 960 hp at 9800 ft. The pilot's all around view was excellent in the open canopy cockpit. The armament consisted of 2x.50 cal machine guns which was at least decent for the 1939-1940 era. It also possessed excellent maneuverability and the C.200's general flying characteristics left little to be desired. Stability in a high-speed dive was exceptional, but it was underpowered and underarmed in comparison to its contemporaries. Early on, there were a number of crashes caused by stability problems, nearly resulting in the grounding of the type, which was ultimately addressed via aerodynamic modifications to the wing.

From the time Italy entered the Second World War on 10 June 1940, until the signing of the armistice of 8 September 1943, the C. 200 flew more operational sorties than any other Italian aircraft. The Saetta saw operational service in Greece, North Africa, Yugoslavia, across the Mediterranean and the Soviet Union (where it obtained an excellent kill to loss ratio of 88 to 15). It's very strong all-metal construction and air-cooled engine made the aircraft ideal for conducting ground attack missions; several units flew it as a fighter-bomber. Over 1,000 aircraft had been constructed by the end of the war. 