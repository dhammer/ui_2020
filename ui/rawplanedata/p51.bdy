NORTH AMERICAN P-51D-25 "MUSTANG"
-= Full weight =-
10120 lb (4590 kg)

-= Best speed and FTH =-
374 mph at sea level (602 kmh)
417 mph at 9000 ft (671 kmh at 2700 m)
441 mph at 22000 ft (710 kmh at 6700 m)

-= Ca time to 16400 ft (5000 m) at Bst1 =-
5.8 min

-= Service Ceiling =-
42000 ft (12800 m)

-= Best turn rate at S.L., 50% fuel and no flaps =-
17.6 sec/lap, 719 ft radius

-= Power ON stall speed at 75% fuel =-
96 mph (154 kmh)

-= Max recommended dive speed (IAS) =-
480 mph (772 kmh)

-= Other notes =-
Improved ailerons. Long range. Agile at high speeds.


The P-51D had improved ailerons and used the Packard V-1650-7 engine, which was a license built Merlin 66 engine and was cleared for 72" manifold pressure once 150 grade fuel became common. 

The North American P-51 Mustang is considered by many to be the finest single seat fighter of WW2. The P-51 was originally designed for the RAF. The prototype, designated NA-73X, first flew in October 1940 with the first production models flying in May 1941. Although the early Allison engined models exhibited many fine characteristics and served as low altitude fighters, reconnaissance and dive bombers, the design came up short in its primary intended role as a high altitude interceptor until the RAF tested the Rolls Royce Merlin 61 and 65 engines in it's Mustang I's. The improvements in performance were enough to impress the USAAF into ordering large numbers of the aircraft. The P-51 in all variants totaled over 15,000 produced, with the P-51D entering service in early 1944 and accounting for 7,956 of that number.
 
With it's outstanding range, the P-51 was able to accompany deep bomber strikes into Germany, a welcome change for the bomber crews who previously left their escorts behind long before entering the German heartland. The Mustang's speed and handling made it a match for anything the enemy put against it, and Mustangs were even credited with kills of the German Me-262 jet fighter late in the war.
 
In the Pacific, the P-51 performed equally as well, though dust and humidity made maintenance a problem. As in Europe, its great range made the Mustang stand out, allowing Mustangs to escort B-29 strikes against the Japanese home islands. 
 
The leading P-51 ace of the war was Major George Preddy Jr. with 26.83 victories. On Christmas day 1944, Preddy shot down two Bf-109's and was chasing a Fw-190 at tree-top level when he was shot down and killed by American anti-aircraft fire meant for the plane he was pursuing.