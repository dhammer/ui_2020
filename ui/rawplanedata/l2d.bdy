The Showa L2D and Nakajima L2D, given the designations: Showa Navy Type 0 Transport and Nakajima Navy Type 0 Transport, were license-built versions of the Douglas DC-3. The L2D series, numerically, was the most important Japanese transport in World War II. The L2D was given the Allied code name Tabby.

Imperial Japanese Navy

The original DC-3s operated by Dai Nippon Koku KK were impressed into Imperial service during the war, serving alongside the license-built L2Ds. The L2Ds served in the Southern Philippines' air groups (Kokutai) in squadrons (Buntai) attached to the 1st, 3rd, 4th, 6th[citation needed], 11th, 12th, 13th and 14th Air Fleets (Koku Kantai) as well as the Combined Fleet (Rengo Kantai) and to the China Area and Southwest Area Fleets. With the large load capacity inherent in all L2D variants, the types were used in all Japanese theaters, as both a passenger and cargo transport, playing an important role in supply of the distant garrisons on the islands of Pacific Ocean and new Guinea. They were also adapted to serve as staff and communications aircraft as well as in the maritime surveillance role. 

Captured

At least one L2D was captured at Zamboanga airfield in May 1945 and later repaired and tested at Clark Field.

RAF Air Chief Marshal Sir Walter Cheshire was in charge of troop and supply airlift in South East Asia after Japan surrendered. Due to lack of resources available he forced the use of Japanese Air Force transport planes and aircrews, creating the RAF Gremlin Task Force (GTF). This included the user of L2D "Tabby" aircraft that supplemented RAF 118 Wing C-47 Dakota aircraft. Japanese aircraft retained their white surrender finish with large blue and white SEAC roundels painted over the green surrender crosses. 


