As the successor to the Pup, the Sopwith Aviation Company produced a fiery, temperamental little biplane, the famous, and notorious, Sopwith F.1 Camel. 

In the hands of an experienced pilot the Camel could out maneuver any contemporary airplane, with the possible exception of the Fokker Triplane. From July 1917, when it reached the Front, until the Armistice, the Camel accounted for no less than 1,294 enemy machines. 

It was the first British type to carry twin Vickers guns; their breeches were enclosed in a 'hump', which gave the Camel its name. 