BREWSTER F2A-3 "BUFFALO"
-= Full weight =-
7733 lb (3508 kg)

-= Best speed and FTH =-
300 mph at sea level (483 kmh)
312 mph at 4500 ft (502 kmh at 1400 m)
320 mph at 15000 ft (515 kmh at 4600 m)

-= Ca time to 16400 ft (5000 m) at Bst1 =-
8.7 min

-= Service Ceiling =-
31000 ft (9400 m)

-= Best turn rate at S.L., 50% fuel and no flaps =-
16.1 sec/lap, 545 ft radius

-= Power ON stall speed at 75% fuel =-
85 mph (137 kmh)

-= Max recommended dive speed (IAS) =-
495 mph (797 kmh)

-= Other notes =-
Low power/weight ratio. Good controls at all speed ranges. Quite tough.


"I remember asking Pappy Boyington about the Brewster Buffalo. I had no sooner finished saying the word 'Buffalo', when he slammed his beer can down on the table, and practicaly snarled, "It was a DOG!" (His emphasis). Then he slowly leaned back in his chair and after a moment quietly said, "But the early models, before they weighed it all down with armorplate, radios and other ****, they were pretty sweet little ships. Not real fast, but the little ****s could turn and roll in a phonebooth. Oh yeah--sweet little ship; but some engineer went and ****ed it up." With that he reached for his beer and was silent again."

Mr. Boyington's statement is quite true. The U.S. F2A-3 is a rather overburdened carrier fighter for the early era of ww2, and carries 240 US gal of fuel. While giving it a great range it also hampers it's performance. This Buffalo does however pack a heavy hit with 4x.50 cals and has ammo to spare. With a 1200 hp engine it also has decent speed for it's era. The Buffalos have a somewhat undeserved reputation after their failure to fight the A6M Zeros at the outbreak of the Pacific war. This is in big part due to the Allied pilots had no idea what they were up against, and tried to turn fight with the Zeros. Had they instead used their superior dive speed and the later developed thach weave of the F4F's, then they would have been more successful. With good wingman tactics and putting the B-339's guns to use, this fighter can put up a good fight. It is adviced against dueling 1v1 against more nimble fighters though. Many Navy pilots regarded the Buffalo as slightly superior to the Wildcat due to it's better all around vision and maneuverability, while others prefered the Wildcat.