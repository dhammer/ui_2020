The Junkers Ju 88 was one of the most versatile and effective combat aircraft of World War II. Its closest counterparts on the Allied side were the Mosquito and Beaufighter. The German aircraft was larger and slower, but nevertheless very effective. 14,676 were built, including a staggering 104 prototypes for its 60 different versions.

Like the Mosquito, the Ju 88 originated as a fast bomber. In 1935 the Luftwaffe had a requirement for a so-called Schnellbomber, which should have a speed of 500km/h with 800kg of bombs. This was much faster than the biplane fighters that then equipped the German fighter units; it was even faster than the first models of the Bf 109 monoplane fighter. For this ambitious goal Henschel proposed the Hs 127, Messerschmitt the Bf 162, and Junkers submitted the designs Ju 85 and Ju 88. Later the Bf 162 achieved some fame when it appeared on German propaganda postcards, but this was disinformation, and the real winner was the Ju 88.

The Ju88 remained unchanged during the Battle of Britain. But the following year the Ju88C was introduced and with its three MG machine guns mounted in the modified solid nose, as well as a 20mm Cannon, and two MG15 machine guns able to be fired from the fusalge it made the Ju88 almost a fighter rather than a bomber.

The C-series was the first version of the Ju 88 to be produced as a fighter aircraft. Work on converting the Ju 88 into a fighter began with the seventh prototype in 1938. This involved replacing the glazed nose of the bomber with a solid nose containing fixed forward firing guns. The resulting aircraft could match the speed of the Bf 110, with three times the range of that aircraft, but production did not become a priority until 1943, by which time the C-series had been replaced by more modern versions.

The C-6 appeared in early 1942. It was based on the improved A-4, and was powered by 1,400hp Jumo 211J-1 or J-2 engines. It carried the same forward firepower as the C-4, along with rear firing dorsal guns and ventral guns. The rear firing ventral gun could be replaced by more forward firing cannon. Production was still limited, but this variant saw wider service, equipping night fighter units and special Zerstörer staffeln in bomber units. The C-6 had a top speed of 310mph, significantly down on the BMW powered aircraft.

Armament:                     
                                Ammo Load          
 
Primary  : 3 x 7.92 MG 17       3000 rpg            
 
Secondary: 3 x .20 mm MG FF/FM  360 rpg   
           1 x 7.92 MG 17       750 rpg   
