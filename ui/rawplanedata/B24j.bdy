In December 29 1939, the prototype of  the Liberator Bomber, XB24  was rolled out and flown for the first time. The chief feature that separated the B-24 from the B-17, was its use of a "Davis" wing. Early flight testing proved the Davis wing superior to it's contemporaries.

Often over shadowed by the B-17, the Liberator achieved undeniable results during the Second World War. The B-24 was produced in far greater quantities than any other American aircraft before or since, with some 18.479 built. Although design for heavy bombing, the B-24 was often used in other roles, such as reconnaissance, anti-ship / sub, rescue, transport,as well as a tanker, making it one of the most versatile aircraft of the war. Despite it's inferior image, the B-24 was superior to the B17 in many ways, carrying a much heavier bomb load, further and faster. The B-24 was also an "all weather" aircraft, which the b-17 was not.  

In WarBirds, the B-24 may now be the players choice, due to increase bomb loads. Players need also be aware that many parts of the B-24, will not take as much damage as a B-17, in particular the wing. Defensive is comparable to the B-17, with an additional front turret in the b-24 J model.

 Armament:                                          Ammo Load          
 
Primary  : 9 x .50 cal MG's (B-24D)                   Varied                   
 	 :10 X .50 Cal MG's (B24J)

Secondary: None       
 
All guns are gunner operated. Gun positions are: Top, Bottom, Nose, Tail, Right (waist), and Left (waist).