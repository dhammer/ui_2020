BREWSTER B-239 "BUFFALO"
-= Full weight =-
5820.2 lb (2640 kg)

-= Best speed and FTH =-
276 mph at sea level (444 kmh)
287 mph at 4500 ft (462 kmh at 1400 m)
289 mph at 20000 ft (465 kmh at 6100 m)

-= Ca time to 16400 ft (5000 m) at Bst1 =-
8 min

-= Service Ceiling =-
32000 ft (9800 m)

-= Best turn rate at S.L., 50% fuel and no flaps =-
12.5 sec/lap, 391 ft radius

-= Power ON stall speed at 75% fuel =-
73 mph (117 kmh)

-= Max recommended dive speed (IAS) =-
495 mph (797 kmh)

-= Other notes =-
Low power/weight ratio but very agile. Good controls at all speed ranges. Quite tough.


"I remember asking Pappy Boyington about the Brewster Buffalo. I had no sooner finished saying the word 'Buffalo', when he slammed his beer can down on the table, and practicaly snarled, "It was a DOG!" (His emphasis). Then he slowly leaned back in his chair and after a moment quietly said, "But the early models, before they weighed it all down with armorplate, radios and other ****, they were pretty sweet little ships. Not real fast, but the little ****s could turn and roll in a phonebooth. Oh yeah--sweet little ship; but some engineer went and ****ed it up." With that he reached for his beer and was silent again."

The B-239 was the Buffalo fighter exported to the Finnish airforce and was one of the early versions that Mr. Boyington loved. It is a well designed lightweight fighter with a good punch of 3x .50 cals and a single .303 cal. Since it is equipped with the same engine as a DC-3, rated at 1000 hp, it lacks in top speed but has nice all around vision, is fairly agile and can easily turn with a Spitfire. It has a very good dive speed, and the Brewsters were recorded to dive at 518 mph IAS without damage. In the Finnish/Russian winter war these planes had a kill/loss ratio of a staggering 32:1, best of ww2, which partially must be credited to the superiorly trained Finnish pilots, but also thanks to this Brewster fighter. The Finns loved their B-239's "Pearl of the sky" and kept them as their frontline fighters until more modern Bf 109G-2's arrived.