Initially known as the T6 Medium Tank, the M4 "General Sherman" would go on to become one of the most important and most produced tanks of WW2 and was only out produced by the Soviet T34. Seeing battle on all fronts and in a plethora of forms, the tank would become one of either fond or awful memory to the soldiers who handled it. 

Often misused and asked to handle tasks usually assigned to heavy tanks in other armies because there was nothing else available. The tank was nimble, reliable, and had superior gun controls - which is part of the reason it was kept around when it should have been replaced. 

The initial production model's armor and 75mm gun were excellent but fluid battlefield conditions would render it weak and finally obsolete barely a year after introduction in Europe. In the East, the M4 would remain peerless throughout the war.
