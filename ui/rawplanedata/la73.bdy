LAVOCHKIN LA-7-3
-= Full weight =-
7217.1 lb (3274 kg)

-= Best speed and FTH =-
374 mph at sea level (602 kmh)
399 mph at 6000 ft (636 kmh at 1500 m)
411 mph at 20000 ft (661 kmh at 6100 m)

-= Ca time to 16400 ft (5000 m) at Bst1 =-
4.9 min

-= Service Ceiling =-
35000 ft (10700 m)

-= Best turn rate at S.L., 50% fuel and no flaps =-
15.4 sec/lap, 683 ft radius

-= Power ON stall speed at 75% fuel =-
95 mph (153 kmh)

-= Max recommended dive speed (IAS) =-
423 mph (681 kmh)

-= Other notes =-
Good roll rate. High wingloading but quite agile.


The La-7-3 version has three Berezin cannons instead of the two SHVak cannons. The Berezin cannon has a higher rate of fire, meaning that the La7 3 version packs a much stronger punch than the normal La 7 version, altough it will run out of ammunition in 1/3 the time.

The La-7 used the same engine as the La-5FN, but had even better top speed due to design improvements, matching a Fw 190D-9 at low altitude while also outturning it. The La-7 also saw structural improvements that lowered it's total weight and strenghtened the wings, increasing the terminal dive speed, climb rate and turning ability even further.

The La-5 had become the heart of the Soviet Air Force in 1943, however the Central Aerohydrodynamics Institute or TsAGI and its designer, Semyon Lavochkin believed they could do better. With replacement of wood parts with metal alloy and using other suggested improvements from pilots in the field, the group had a working prototype (La-120) by November of 1943 and a working model in service at the front lines the following Spring.

The La-7 amassed a superior combat record and was so versatile that it even did double duty as a testbed for Russia's fledgling rocket propulsion systems. The La-7 was produced in large numbers (5,753) and continued to improve as it was being produced. The last few batches of La-7's were fitted with 3 20mm B-20 guns. There was no designation alteration for this version but for simplicities sake we are calling it the La-7 III.