DE HAVILLAND DH.98 "MOSQUITO" NF.30
-= Full weight =-
20511 lb (9304 kg)

-= Best speed and FTH =-
344 mph at sea level (554 kmh)
392 mph at 14000 ft (631 kmh at 4300 m)
408 mph at 25000 ft (657 kmh at 7600 m)

-= Ca time to 16400 ft (5000 m) at Bst1 =-
7.0 min

-= Service Ceiling =-
36000 ft (11000 m)

-= Best turn rate at S.L., 50% fuel and no flaps =-
18.4 sec/lap, 812 ft radius

-= Power ON stall speed at 75% fuel =-
101 mph (163 kmh)

-= Max recommended dive speed (IAS) =-
450 mph (724 kmh)

-= Other notes =-
Good controls at all speeds. Slow roll rate. 


This night fighter late war version used the two stage Merlin 72 engines which, compared to the NF.II's engines, improved the flight envelope by around 10 000 ft. The NF.30 has a full throttle height (FTH) at 25000 ft and was armed with 4x 20 mm Hispano cannons.

The "Wooden Wonder" was rejected as a concept by Britain's Air Marshals, and had to be designed and built in an English manor house and test flown off an adjoining farm field. It became possibly the most successful and certainly most versatile British aircraft of WW2.

It served as Photo-recon, day fighter, night fighter, bomber, mine layer, torpedo bomber, tank buster, pathfinder, and submarine hunter. What's left?

Sir Geoffrey de Havilland proposed his wooden speed bomber in in 1938. When he was turned down, he said "We'll do it anyway". With the encouragement of Air Chief Marshal Sir Wilfred Freeman, de Havilland set up shop in Salisbury Hall and set to work. The prototype flew in November 1940 and went into production.

Wood was not a scarce commodity like the metals used in other planes, and much of the skilled workforce came from otherwise underemployed furniture craftsmen, making the Mosquito doubly valued, as much for it's manufacturing efficiency as it's performance.

7,781 Mosquitos of all types were eventually built. At near 400 mph level speed, it had the ability to penetrate deep into enemy territory while avoiding interception. In 1943, an 11am address in Berlin by Goering in which it was boasted that no enemy bomber would ever reach Berlin unscathed was interrupted by three Mosquitos from 105 squadron who dropped their bombs on the city and returned safely to English soil. At 4pm that same day, a similar address by Goebbels was interrupted in the same manner. It was an outstanding success, almost stage-managed by Goering himself, that gave the Mosquito a moral advantage it never relinquished.