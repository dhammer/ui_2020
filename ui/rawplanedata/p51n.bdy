The North American P-51 Mustang is considered by many to be the finest single seat fighter of WW2. The P-51 was originally designed for the RAF. The prototype, designated NA-73X, first flew in October 1940 with the first production models flying in May 1941. Although the early Allison engined models exhibited many fine characteristics and served as low altitude fighters, reconnaissance and dive bombers, the design came up short in its primary intended role as a high altitude interceptor until the RAF tested the Rolls Royce Merlin 61 and 65 engines in its Mustang I's. The improvements in performance were enough to impress the USAAF into ordering large numbers of the aircraft. The P-51 in all variants totaled over 15,000 produced, with the P-51D entering service in early 1944 and accounting for 7,956 of that number.
 
With its outstanding range, the P-51 was able to accompany deep bomber strikes into Germany, a welcome change for the bomber crews who previously left their escorts behind long before entering the German heartland. Its speed and handling made it a match for anything the enemy put against it, and Mustangs were even credited with kills of the German Me-262 jet fighter late in the war.
 
In the Pacific, the P-51 performed equally as well, though dust and humidity made maintenance a problem. As in Europe, its great range made the Mustang stand out, allowing Mustangs to escort B-29 strikes against the Japanese home islands. 
 
The leading P-51 ace of the war was Major George Preddy Jr. with 26.83 victories. On Christmas day 1944, Preddy shot down two Bf-109's and was chasing an Fw-190 at tree-top level when he was shot down and killed by American anti-aircraft fire meant for the plane he was pursuing.
 
In WarBirds, the P-51 remains an outstanding fighter, though some of its best points are only realized in large organized scenarios. One of the favorites of the "Boom and Zoom" school of fighting, the P-51 is the fastest plane in the game at most altitudes, and its speed allows it to strike so quickly that many of it's victims never see it coming. Escaping trouble is as simple as pointing the nose down and running away from pursuers, a habit that has given the P-51 the derisive nickname "Runstang" among its sometimes frustrated opponents.
     
Armament:                     
                                   Ammo Load          
 
Primary  : 2 x .50 cal MG's        400 rpg            
 
Secondary: 4 x .50 cal MG's        270 rpg            
 
All guns are wing mounted.
 
 
The "Ammo Load" is based on the historic normal operational ammo load for the plane/weapon(s) in question.