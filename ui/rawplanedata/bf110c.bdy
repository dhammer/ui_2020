MESSERSCHMITT BF 110C-4
-= Full weight =-
15300 lb (6940 kg)

-= Best speed and FTH =-
286 mph at sea level (460 kmh) [1 min limit]
305 mph at 5400 ft (491 kmh at 1650 m) [1 min limit]
318 mph at 15000 ft (512 kmh at 4600 m)

-= Ca time to 16400 ft (5000 m) at Bst1 =-
8.7 min

-= Service Ceiling =-
32000 ft (9800 m)

-= Best turn rate at S.L., 50% fuel and no flaps =-
16.1 sec/lap, 534 ft radius

-= Power ON stall speed at 75% fuel =-
80 mph (129 kmh)

-= Max recommended dive speed (IAS) =-
435 mph (700 kmh)

-= Other notes =-
Not very agile but packs a good punch for it's era.


The Bf 110C-4 is equipped with two Daimler-Benz DB 601B engines, which are the similar to the engines used by the Bf 109E-3, but with a different propgear ratio. It is armed with four 7.92 mm MG's and two 20 mm cannons. 

The Messerschmit Bf-110 was an attempt at compromise. Conceived in 1934 and first flown as a prototype in 1936, the Bf-110 was intended as a strategic long range heavy fighter, or "Zerstorer".
 
The first production model, the Bf-110B, entered service in 1938. The Bf-110C joined the Luftwaffe in time for the invasion of Poland in 1939 and served as both a fighter and fighter bomber with great success as Germany drove across Europe. But in the Battle of Britain, when acting as bombers escorts, the pilots of the Bf-110 found they could not compete with the single engined Spitfires and Hurricanes of the RAF. Too heavy and too slow, it could not outrun the British fighters, nor could it outturn them. Relegated to ground attack and night fighter roles, it served admirably, and was the main German night fighter, equipped with radar to intercept nighttime bomber raids.
 
The Bf-110's reputation never recovered from the mauling it received over England, but it remained a versatile and effective weapon. The most successful Bf-110 pilot was Major Heinz-Wolfgang Schnaufer, who received the Diamonds to the Knight's Cross while claiming 121 kills as a night fighter.