This version of the Sherman used a welded hull nearly identical to the M4, but with a pair of vented armored grates on the rear hull deck. The M4A2 tanks used the GM 6046 twin diesel. This version was produced with all the improvements the other types got, like the large hatch hull. This version would see very limited combat in US hands, most being the British, with some 75 tanks going to the Russians and USMC. 

The M4A2 Shermans were equipped with a General Motors 6046 engine (2x GM 6-71 General Motors Diesel engines); they have a welded hull, there were 75mm and 76mm versions only. The users were : USSR, USMC (in the Pacific Theater of Operations), France, Britain, Poland. No US Army combat use

Armament:                                           

Primary:  
76mm Gun M1A1, M1A1C or M1A2 and .30in coaxial MG in turret, .50in MG in AA mount on turret roof, 0.30in MG in hill front, 2in Mortar M3 (smoke) in turret 

Ammunition load:

This tank has a 47° glacis, large hatches and wet ammunition stowage. It has a T23 turret and a 76mm main gun. 
