This Triplane had an excellent rate of climb and could match the Camel for maneuverability. These merits outweighed its lack of speed at combat altitude. For two months the pilots of Jagdgeschwader Nr. 1, the Richthofen 'Circus', ably demonstrated its capabilities. 

In late October 1917 Lieutenants Gontermann and Pastor were killed when their DR.-Is broke in the air, and the DR.-I was withdrawn from operations. The triplane was issued with modified wings in December 1917. The failures continued but, to a lesser extent. The DR.-I never recovered from this setback and was not supplied to many Jagdstaffeln. 

The cause of the wing failures was said to be from poor quality control rather than deficiencies in design. However, the failures always occurred in the upper wing of the aircraft. In the two most notable accidents of Gontermann and Pastor, the stripping of the top wing was almost total. 

During NACA investigation conducted in 1929 it was found that the upper wing of biplanes carried a higher wing loading than the lower wing. At high speed level flight, the upper wing could carry as much as 2.55 times more load than the lower wing. The increased wing loading, and poor workmanship could explain the failure of the upper wings of the DR.-1. 

Some 320 DR.-Is were built before production finished in May 1918. 