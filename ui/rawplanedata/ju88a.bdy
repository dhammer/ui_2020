The Junkers Ju-88 was probably the most versatile aircraft of WW2 and certainly the best bomber the Germans possessed. The number of variants is staggering and a partial catalogue includes the original "Schnellbomber" or fast bombers, high alt-
itude bombers, The "Zerstorer" or heavy fighters, radar equipped night fighters, torpedo bombers, recon aircraft, tank busters, and dive bombers as well as being the lower half of the late-war "Mistel" combination where a Bf-109 or Fw-190 was attached above a bomb laden unmanned Ju-88 and steered it towards a gliding release at the target.
 
A prototype that flew in June 1938 led to the first production models in Mid 1939. The first combat mission was in September 1939 against several British warships, an unspectacular sortie whose highlight was the bouncing of a 500 lb bomb off the HMS Hood. The Ju-88 was nonetheless a spectacularly successful aircraft. It was the main offensive punch of the Luftwaffe until strategic concerns pressed Germany's air arm into defensive roles. 
 
By the Spring of 1941, the Ju-88-A4 was the main airframe type and served as the basis for subsequent versions. Payloads varied substantially depending on what role was being performed. At least 14,980 Ju-88's were produced during the course of the war.
 
In WarBirds, the Ju-88 remains an excellent bomber and one of only three planes that carry torpedoes, with the Ju-88 carrying two. It can carry up to four 1000 lb bombs and is capable of level or dive bombing. As the only German bomber represented, it should see extensive scenario use.
   
Armament:                     
                                   Ammo Load         
 
Primary  : 1 x 7.9mm MG            750 rpg           
 
Secondary: None
 
Gunners: 3 x 7.9mm MG
         1 x 13mm MG (top)
 
	Gunner positions are: Top, Nose, Bottom.