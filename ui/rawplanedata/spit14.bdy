SUPERMARINE SPITFIRE XIVe
-= Full weight =-
8488 lb (3850 kg)

-= Best speed and FTH =-
371 mph at sea level (597 kmh)
417 mph at 9700 ft (671 kmh at 3000 m)
457 mph at 23100 ft (735 kmh at 7000 m)

-= Ca time to 16400 ft (5000 m) at Bst1 =-
5.8 min

-= Service Ceiling =-
41000 ft (12500 m)

-= Best turn rate at S.L., 50% fuel and no flaps =-
13.2 sec/lap, 576 ft radius

-= Power ON stall speed at 75% fuel =-
89 mph (143 kmh)

-= Max recommended dive speed (IAS) =-
470 mph (756 kmh)

-= Other notes =-
CCW-rotating propeller. Agile with a gentle stall behaviour.


Being powered by the Rolls-Royce Griffon 65 engine and using 150 grade fuel, the Mk. XIVe had an output of an astounding 2200 hp at 9700 ft, and a power/weight ratio which was unequalled in WW2. This fighter is not a beast but rather a monster. When using it's +21 lbs WEP the Spitfire XIVe can run like a P-51D, climb with the very best and hold a sustained turn better than all other late war fighters. It's weakness in the war was that it lacked the fuel capacity to escort bombers deep into Germany and back.

In addition to being one of the most beautiful fighters ever built, the Supermarine Spitfire was one of the most robust designs. It had a great wing efficiency due to the eliptical shape of it's wings, resulting in less induced drag effects. With the same airframe that first flew as a prototype in 1936, the Spitfire remained in production throughout WW2, the only Allied fighter to do so. Improvements were mainly to the engine, with the basic shape remarkably able to accommodate the increased size and power of the ever larger Merlins. Combining speed, firepower, and agility into one potent package, the Spit with it's distinctive elliptical wings is always remembered as the plane that won the Battle of Britain... though the Hawker Hurricane carried most of the load.
 
The Spitfire Mk1a was in service at the outbreak of WW2. The Spit V became available in March 1941, and the Spit IX, the most produced variant at 5,665 planes, was introduced in mid 1942.
 
James "Johnny" Johnson was the highest scoring British Spitfire pilot with 38 victories.