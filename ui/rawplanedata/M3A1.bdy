The M3A1 Scout Car was used by cavalry units of the US Army in its intended cavalry role during the North African Campaign and the invasion of Sicily, being employed for reconnaissance, screening and as an armored command vehicle. 

The M3A1 was fast and reliable, making it popular with its crews. It was considered a major disappointment in US Army service, primarily due to its poor offroad performance, but also its lack of overhead protection. Cavalry units were forced to supplement it with M2 and M3 Half-tracks. 

Throughout 1943, most US Army units replaced the M3A1 with the M8 armored car and the similar M20 Utility Car, although the M3A1 was retained for rear area security and convoy escort duties. A small number of M3A1s were employed in Normandy. A few M3A1s were used by the US Marine Corps in the Pacific theater, but none saw combat

Armament:                                           

Primary:  
1 x 0.50 Browning M2 heavy machine gun
1 x 0.30 Browning M1919A4 machine gun

Bomb Load: