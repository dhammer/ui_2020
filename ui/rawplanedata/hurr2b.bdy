Design of the Hawker Hurricane began in 1933 as Britain looked for its first monoplane fighter. A prototype was flown in November of 1935 and production of the Hurricane began in July 1936 with the first squadrons forming by the end of 1937. All Hurricanes had fabric covered wings until the Autumn of 1939 when the installation of the 1260 hp Merlin XX engine and heavier armament marked the Hurricane II series of variants. The IIa kept the old wings with the newer engine while the IIb had 12 browning machine guns mounted in the new all metal stressed skin wings. The IIc used four 20mm Hispano-Suiza drum fed cannon and the IId was outfitted with a pair of 40mm anti-tank cannon. 

The memorable Spitfire gets most of the credit for winning the Battle of Britain, but the Hawker Hurricane was the real workhorse of the conflict. The 1715 Hurricanes that served in the battle accounted for four fifths of the enemy aircraft destroyed. The Hurricane expanded it's role into the Mediterranean, North Africa, and the Middle East during 1940, and into the Far East in 1941. It saw air-to-ground action for the most part as later generations of fighters took up the air battle. A total of 14,223 Hurricanes were produced.
 
The top scoring Hurricane ace in WW2 was Squadron Leader Marmaduke Thomas St. John Pattle, a South African who served in North Africa and later in Greece. He had 35 victories while flying the Hurricane. Some estimates give him 50 kills altogether which would also make him the highest scoring RAF ace of the war.

Hurricane Mk IIA Series 2 was equipped with new and slightly longer propeller spinner and new wing mounting 12 x .303 in (7.7 mm) Browning machine guns. The first aircraft were built in October 1940 and were renamed Mark IIB in April 1941.

Armament:                     
                                   Ammo Load          
 
Primary  : 6 x .303 MG's           350 rpg            
 
Secondary: 6 x .303 MG's           350 rpg            
 
All guns are wing mounted.
 
 
The "Ammo Load" is based on the historic normal operational ammo load for the plane/weapon(s) in question.