The M5 "Stuart" (also "General Stuart") was an extension of the original M3 Stuart Light Tank line of 1941 and brought about by the American military relocation of vital war-making supplies - namely the Continental aero engine used in the M3. This initiative begat a modified Stuart light tank initially known as the "M4" though later changed to "M5" to differentiate it from the classic M4 Sherman medium tank. The British Army continued naming their American tanks and the M5 was known as "Stuart VI"

As completed, the M5 now showcased a paired set of Cadillac V8 296-horsepower automobile engines which forced a reworking of the engine deck at the rear of the hull. The engines were coupled to individual Hydra-Matic transmission systems running through a 2-speed transfer case and provided improved running performance and noise reduction over the original Continental installation. A new front hull design, based on the M3A3 Stuart, was implemented and brought about improved frontal ballistics protection.

Armament:                                           
1 x 37mm M6 main gun
1 x 0.30 caliber Browning M1919A4 coaxial machine gun.
1 x 0.30 caliber Browning M1919A4 Anti-Aircraft (AA) machine gun on turret side (operated externally). 

Ammunition load:
103 x 37mm projectiles
7,500 x 0.30 caliber ammunition
