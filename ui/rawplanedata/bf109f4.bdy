The Bf-109 in all its variants was produced in larger numbers 
than any other fighter in history, with over 30,000 built. By 
itself, that number speaks of the success of this aircraft. 
Also known as the Me-109 after its creator, Dr. Willy Messers-
chmitt, the first Bf-109 prototype took flight in 1935 and the 
final service variant was phased out by the Spanish Air Force 
in 1967...a career of over 30 years. 
 
Of the types modeled in WarBirds, the 109E began appearing in 
1939 and participated in the Battle of Britain. The Bf-109F4 
entered service early in 1942 and introduced more drag reduct-
ion to the design as well as moving the wing MG's to the nose. 
The G6 model appeared in December 1943 with the G6/R6 a more 
heavily armed subvariant of that model, and the Bf-109K4, a 
high speed high altitude interceptor version, began arriving 
in October 1944.
 
The Bf-109 incorporated several new advances in structural de-
sign and aerodynamics when it was built. 5 years after its 
first introduction, it remained superior to any opposing fight-
er, and its many variations kept it abreast of the likes of the 
Spitfire MkIX and other allied planes throughout the war. How-
ever, the 109's success was bolstered in part by the large 
corps of extremely experienced pilots flying the plane. 
 
The leading fighter ace of all time, Oberst Erich Hartmann, 
flew the Bf-109 for all of his 352 aerial victories, and there 
were literally dozens of others with well over 100 kills in 109 
variants.
 
In WarBirds, the 109's superior climb rate serves it well if 
used for vertical maneuvering. Don't turn this bird flat. It 
carries respectable armament but not as much ammo as other 
planes, and as the leading German aces found...shooting in 
close is the best way to get the most out of its guns. This 
plane will be an average performer in most hands...and is not a 
bad choice generally, but as in the war itself, it takes an 
expert to really make the 109 the great weapon it was.
   
Armament:                     
                                   Ammo Load          
 
Primary  : 2 x 7.9mm MG's          500 rpg             
 
Secondary: 1 x 20mm cannon         200 rpg             
 
MG's are fuselage mounted. Cannon is spinner mounted.
 
 
The "Ammo Load" is based on the historic normal operational 
ammo load for the plane/weapon(s) in question. 
