Based on a glider, the Messerschmitt Me 163 landed on a small skid running down the centerline of the plane. This might work for a slow plane, but the 163 was nothing of the sort. Landings were often very hard with the pilots injuring their backs, and minor upsets on landing could cause the plane to tip over and clip a wingtip and go spinning down the field. 

Even if the pilot did manage to land the plane OK, it was immobile on the field until it could be towed away. Another major concern was the short flight time. With only 8 minutes of powered flight, the plane truly was an interceptor (or simply interceptor) is a type of fighter aircraft designed specifically to intercept and destroy enemy aircraft, particularly bombers. 

A number of such aircraft were built in the period starting just prior to World War II and ending in the late 1960s, when they became less important due to the shifting of the strategic bombing role to ICBMs. 

Nevertheless the plane was astonishing in flight. After take-off from a dolly it would be going over 200 mph (300 km/h) at the end of the runway, at which point it would pull up into an 80 degree climb all the way to the bomber's altitude. It could go even higher if need be, reaching 40,000 ft (12,000 m) in an unheard-of three minutes. Once there it would level off and quickly accelerate to speeds around 550 mph (880 km/h) or faster.
