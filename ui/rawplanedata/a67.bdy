[IMG]A67USAC\texture\USAC.tga 0 0 254 254[/IMG]

In 2006 US Aircraft Corporation released the A-67 for its first test flight. The A-67 is designed to be cost effective, easy to maintain and versatile for use in various missions. It has the ability to meet close air support needs without the expense, training and maintenance current markets provide. Some of its key cost saving features is the skin, large fuel capacity, excellent load and horse capabilities and a lighter yet durable airframe.

In late 2007, iEntertainment and it's partners developed a Simulation Demonstration of the A-67 for US Aircraft Corporation as a part of their 2007 Dubai Airshow presentation.
