The Grumman F6F was developed as a backup to the Vought F4U, should that design encounter any setbacks.  It was a precaution that paid dividends as the F6F was to become the Navy's workhorse fighter in WWII.  With over 12,200 F6Fs produced, more than 5000 Japanese aircraft destroyed, and an astounding 19:1 kill to loss ratio, the F6F proved to be one of the most redoubtable fighters of WWII.
 
The F6F was built in two major variants, the F6F-3 and the F6F-5. The F6F-3 entered service at the beginning of 1943. 4,423 F6F-3s were built before that variant was replaced by the F6F-5 in mid 1944.  The F6F-5 would become the major variant with 7870 produced.  It incorporated a number of minor improvements, but overall, little was changed in the design.  The canopy was redesigned, rocket rails were added, more armor was added, the tail was structurally reinforced, and water injection was added.
 
The U.S. Navy's highest scoring ace and the third leading American ace, Commander David McCampbell, scored all 34 of his victories while flying the Hellcat.  This was the most victories by any American pilot in a single tour of duty.  He was also the only American to down 5 or more planes in a single day on two separate occasions, including 9 victories in a single sortie, a figure unmatched by any other Allied pilot.
 
The F6F was a good design, although not exceedingly superior in any one area.  However, it was a plane of few vices and its reliability and durability were not insignificant factors in its success.
 
In WarBirds, the plane's biggest handicap is probably its poor rearward visibility.  Aside from that, the Hellcat has few vices.  Its generally good in all categories but not superior in any single trait.
   
Armament:                     
                                   Ammo Load          
 
Primary  : 2 x .50 cal MG's        400 rpg            
 
Secondary: 4 x .50 cal MG's        400 rpg            
 
 
The "Ammo Load" is based on the historic normal operational ammo load for the plane/weapon(s) in question.