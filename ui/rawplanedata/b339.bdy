BREWSTER B-339 "BUFFALO"
-= Full weight =-
7253 lb (3290 kg)

-= Best speed and FTH =-
290 mph at sea level (467 kmh)
302 mph at 4500 ft (486 kmh at 1400 m)
295 mph at 17000 ft (475 kmh at 5200 m)

-= Ca time to 16400 ft (5000 m) at Bst1 =-
10.2 min

-= Service Ceiling =-
31000 ft (9400 m)

-= Best turn rate at S.L., 50% fuel and no flaps =-
15.5 sec/lap, 521 ft radius

-= Power ON stall speed at 75% fuel =-
83 mph (134 kmh)

-= Max recommended dive speed (IAS) =-
495 mph (797 kmh)

-= Other notes =-
Low power/weight ratio. Good controls at all speed ranges. Quite tough.


"I remember asking Pappy Boyington about the Brewster Buffalo. I had no sooner finished saying the word 'Buffalo', when he slammed his beer can down on the table, and practicaly snarled, "It was a DOG!" (His emphasis). Then he slowly leaned back in his chair and after a moment quietly said, "But the early models, before they weighed it all down with armorplate, radios and other ****, they were pretty sweet little ships. Not real fast, but the little ****s could turn and roll in a phonebooth. Oh yeah--sweet little ship; but some engineer went and ****ed it up." With that he reached for his beer and was silent again."

This is the British Navy/land version of the Buffalo. It has a lower top speed than the F2A-3 due to fielding a 1100 hp engine, but is lighter since it carries 480 lb less fuel. The weaker engine means it has the lowest climb rate of the Buffalo's though. The Buffalos have a somewhat undeserved reputation after their failure to fight the A6M Zeros at the outbreak of the Pacific war. This is in big part due to the Allied pilots had no idea what they were up against, and tried to turn fight with the Zeros. Had they instead used their superior dive speed and the later developed thach weave of the F4F's, then they would have been more successful. With good wingman tactics and putting the F2A-3's guns to use, this fighter can put up a good fight. It is adviced against dueling 1v1 against more nimble fighters though. Many Navy pilots regarded the Buffalo as slightly superior to the Wildcat due to it's better all around vision and maneuverability, while others prefered the Wildcat.