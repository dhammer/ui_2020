The Vickers Wellington was a British twin-engined, long range medium bomber designed in the mid-1930s at Brooklands in Weybridge, Surrey, by Vickers-Armstrongs' Chief Designer, Rex Pierson in response to specification B.9/32. Issued in the middle of 1932, this called for a twin-engined day bomber of higher performance than any previous design. It was widely used as a night bomber in the early years of the Second World War, before being superseded as a bomber by the larger four-engined "heavies" such as the Avro Lancaster.

The Wellington continued to serve throughout the war in other duties, particularly as an anti-submarine aircraft. It was the only British bomber to be produced for the duration of the war and was still first-line equipment when the war ended. The Wellington was one of two bombers named after Arthur Wellesley, 1st Duke of Wellington, the other being the Vickers Wellesley.

Type 416 Wellington Mark IC


The first main production variant was the Mark IC which added waist guns to the Mark IA. A total of 2,685 were produced. The Mark IC had a crew of six; a pilot, radio operator, navigator/bomb aimer, observer/nose gunner, tail gunner and waist gunner. A total of 2,685 were built at Weybridge, Chester and Blackpool.

Armament:                     
                                   Ammo Load          
 
Primary  :   2 x 303 Cal Mk II Dual        3000                   
 
Secondary: 2 x 303 Cal Mk II                 1000            
       
Various bomb loads

All guns are gunner operated. Gun positions are: Top, Bottom, Nose, Tail, Right (waist), and Left (waist).
