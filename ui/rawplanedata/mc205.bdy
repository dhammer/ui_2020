MACCHI C.205 "VELTRO"
-= Full weight =-
7513.4 lb (3408 kg)

-= Best speed and FTH =-
347 mph at sea level (558 kmh) [1 min limit]
376 mph at 7000 ft (605 kmh at 2100 m) [1 min limit]
411 mph at 23000 ft (661 kmh at 7000 m) [1 min limit]

-= Ca time to 16400 ft (5000 m) at Bst1 =-
5.2 min

-= Service Ceiling =-
38000 ft (11600 m)

-= Best turn rate at S.L., 50% fuel and no flaps =-
16.9 sec/lap, 667 ft radius

-= Power ON stall speed at 75% fuel =-
89 mph (143 kmh)

-= Max recommended dive speed (IAS) =-
500 mph (805 kmh)

-= Other notes =-
Good controls at all speeds. Reduced torque effects due to designed difference between the left vs right wing span.


The Macchi C.205 Veltro (Greyhound) was built by the Aeronautica Macchi. The 205 was a development of their earlier C.202 Folgore, mounting a more powerful Dailmer-Benz DB 605 engine. It is considered one of the best fighters to be used by any airforce during the war. Looking to improve the performance of all their front line fighters, in 1942 the Regia Aeronautica decided to upgrade all current designs to use the new 1,475 hp DB 605 engine, license produced in Italy as the Fiat RA.1050 Tifone. Fighter manufacturers were invited to enter versions of their designs with this engine as the so-called "5 series" planes, and were provided with imported 605's for prototype use. All of the designs skipped over holes in their existing numbering to use a 5 in the name, with the Macchi becoming the C.205 (instead of C.203).

Macchi had a natural advantage over the competition, as their then-current C.202's used the DB 601 engine, which was "plug compatible" with the 605. The C.205 could thus be in the air almost immediately. The Fiat G.55 Centauro and the Reggiane Re.2005 Sagittario were modified versions of much earlier designs, and would take some time to get into service.

In testing the Centauro and Sagittario proved to be better performers at higher altitudes due to their larger wings. The Veltro used the same wing as the earlier Folgore, but the weight was increased and it thus had an increased wingloading. The Veltro had performance more in line with German designs with their higher wing-loadings, and was at its best at 23000 ft where it could reach 411 mph. Plans were made to produce both the G.55 and Re.2005, but they also put the Veltro into production immediately.

Veltros reached the front-line groups in June 1943, and first flew in combat escorting bombers attacking Allied naval forces off Sicily. The planes fought continually over the next two months, and proved to be as good as any of the allied planes they met. In one instance on the 2nd of August, six Veltros attacked twenty Lockheed P-38's and Curtiss P-40's, shooting down six of them for the loss of only one of their planes.

In August the Italian government made peace with the Allies, by which time the Regia Aeronautica had a total of 66 Veltros. Six of these flew to allied airfields to serve with the Italian Co-Beligerant Air Force, while another 29 reached northern airfields to be used by the Republican Socialist Italian Air Force.