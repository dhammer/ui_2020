MESSERSCHMITT BF 109K-4 "KURFURST"
-= Full weight =-
7495.7 lb (3400 kg)

-= Best speed and FTH =-
371 mph at sea level (597 kmh)
379 mph at 2000 ft (610 kmh at 600 m)
435 mph at 23000 ft (700 kmh at 7000 m)

-= Ca time to 16400 ft (5000 m) at Bst1 =-
4.9 min

-= Service Ceiling =-
41000 ft (12500 m)

-= Best turn rate at S.L., 50% fuel and no flaps =-
14.4 sec/lap, 551 ft radius

-= Power ON stall speed at 75% fuel =-
87 mph (140 kmh)

-= Max recommended dive speed (IAS) =-
470 mph (756 kmh)

-= Other notes =-
Good roll rate. Elevator quickly becomes heavy with increasing speed. 20 minutes methanol-water injection capacity per sortie.


The K-4 is the ultimate version of the Bf 109 series. An added feature is the choice of using the heavy 30 mm MK/108 cannon instead of the standard 20 mm cannon. The DB 605DB engine can produce 1846 hp at 2000 ft with MW50 injection and B4 fuel, which gives the K-4 a power/weight ratio similar to the Spitfire XIV, and can definitely match the Spit XIV in a sustained low n' slow turn fight.

The Bf-109 in all its variants was produced in larger numbers than any other fighter in history, with over 30,000 built. By itself, that number speaks of the success of this aircraft. Also known as the Me-109 after its creator, Dr. Willy Messerschmitt, the first Bf-109 prototype took flight in 1935 and the final service variant was phased out by the Spanish Air Force in 1967, a career of over 30 years. 
 
Of the types modeled in WarBirds, the 109E began appearing in 1939 and participated in the Battle of Britain. The Bf-109F4 entered service early in 1942 and introduced more drag reduction to the design as well as moving the wing MG's to the nose. The G6 model appeared in December 1943 with the G6/R6 a more heavily armed subvariant of that model, and the Bf-109K4, a high speed high altitude interceptor version, began arriving in October 1944.
 
The Bf-109 incorporated several new advances in structural design and aerodynamics when it was built. 5 years after its first introduction, it remained superior to any opposing fighter, and its many variations kept it abreast of the likes of the Spitfire MkIX and other allied planes throughout the war. However, the 109's success was bolstered in part by the large corps of extremely experienced pilots flying the plane. 
 
The leading fighter ace of all time, Oberst Erich Hartmann, flew the Bf-109 for all of his 352 aerial victories, and there were literally dozens of others with well over 100 kills in 109 variants.