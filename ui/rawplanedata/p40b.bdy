CURTISS P-40B "TOMAHAWK"
-= Full weight =-
7559.4 lb (3429 kg)

-= Best speed and FTH =-
303 mph at sea level (488 kmh)
344 mph at 13600 ft (554 kmh at 4150 m)
352 mph at 17000 ft (566 kmh at 5200 m)

-= Ca time to 16400 ft (5000 m) at Bst1 =-
8.4 min

-= Service Ceiling =-
31000 ft (9400 m)

-= Best turn rate at S.L., 50% fuel and no flaps =-
14.6 sec/lap, 498 ft radius

-= Power ON stall speed at 75% fuel =-
80 mph (129 kmh)

-= Max recommended dive speed (IAS) =-
470 mph (756 kmh)

-= Other notes =-
Rugged structure.


The P-40B fielded an inline Allison V-1710-33 engine instead of the radial engine of the P-36. This resulted in a higher top speed and better performance at altitude. This version saw a weight increase of ~1300 lb compared to it's predecessor, which took a toll on it's sustained turn performance and climb rate, but it was still a fairly agile bird. It was equipped with pilot armor and had a better top speed and armament than the P-36C/Hawk 75.

When it entered production in 1940, the P-40 was already outclassed by such fighters as the British Spitfire and German Bf-109. Still, P-40's served in every allied air force and in every theatre during WW2. As it was, the P-40 never caught up with the state-of-the-art, but its toughness and rugged construction made it easy to service on frontlines, and proved of great value as a successful tactical fighter-bomber. The prototype first flew in October 1939 and 13,738 P-40's of all types were built.
 
Probably the most famous unit flying P-40's was Gen. Claire Chennault's American Volunteer Group (AVG) fighting the Japanese in China in 1941 and 42. The P-40D saw the introduction of a new series of Allison engines, resulting in a shorter nose and deeper radiator. With the P-40E, the fuselage guns were discarded and standard armament became six .50 cal MG's.

The top scoring P-40 pilot was Flt. Lt. Clive "Killer" Caldwell of 250 Squadron Raf. An Australian, he flew Tomahawks and Kittyhawks in North Africa and finished the war with 27 kills.