The Halberstadt CLII was designed as a two-seat fighter to act as an escort for the earlier and heavier C-type recce aircraft the Halberstadt CLII first appeared in 1917 and soon entered service with the Schutzstaffeln (protection flights) of the German Aviation Service. 

On 6 September 1917, 24 Halberstadt CLII aircraft attacked and decimated British troops crossing the bridges over the Somme at Bray and St. Christi. As a result they were then designated Schlachstaffeln (battle fighters) for future close-support roles.
