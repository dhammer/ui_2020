Originally designed as a single engined transport and flown in 1930, the Ju-52 was redesigned with 3 engines as the Ju-52/3m to serve as a bomber in 1932. It was unsuccessful in the bombing role, however, and was relegated mostly to transport duties. In that role, it became one of the classic aircraft of the war, forming the backbone of Luftwaffe air transport operations.
    
Rugged, reliable, and easy to fly, the Ju-52/3m stayed in service through the war, even when later, better designs were available.

Heavy Ju-52/3m losses through the air assault campaigns in the Netherlands and Crete left the Luftwaffe transport arm woefully undermanned, and this shortage made itself felt during the invasion of Russia, where air transport was often the sole means of resupply.

Total output of the design was 4,845 aircraft. In addition to the transport role, Ju-52/3m's served in minesweeper, recon, glider tug, and the aforementioned bomber roles.

In WarBirds, the primary role of the Ju-52/3m will be transporting paratroops for airfield capture. It carries light defensive armament in the form of three 7.9mm MG's for waist and dorsal gunners.