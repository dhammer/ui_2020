SUPERMARINE SPITFIRE Ia - 87 OCTANE FUEL
-= Full weight =-
6050 lb (2744 kg)

-= Best speed and FTH =-
275 mph at sea level (443 kmh)
339 mph at 17000 ft (546 kmh at 5200 m)
317 mph at 25000 ft (510 kmh at 7600 m)

-= Ca time to 16400 ft (5000 m) at Bst1 =-
5.7 min

-= Service Ceiling =-
34000 ft (10400 m)

-= Best turn rate at S.L., 50% fuel and no flaps =-
13.4 sec/lap, 479 ft radius

-= Power ON stall speed at 75% fuel =-
75 mph (121 kmh)

-= Max recommended dive speed (IAS) =-
450 mph (724 kmh)

-= Other notes =-
Limited to +6.25 lbs boost. Negative G's engine cutout. Agile with a gentle stall behaviour.


The Mk.I became famous in the Battle of Britain. While there are some early war fighters that can turn inside it, the Spit I is still very competitive in a turn fight. This coupled with an excellent top speed and 8x .303 cal Mg's makes it a dangerous opponent. It's drawback is that the Merlin III engine loses in performance above 15000 ft.

In addition to being one of the most beautiful fighters ever built, the Supermarine Spitfire was one of the most robust designs. It had a great wing efficiency due to the eliptical shape of it's wings, resulting in less induced drag effects. With the same airframe that first flew as a prototype in 1936, the Spitfire remained in production throughout WW2, the only Allied fighter to do so. Improvements were mainly to the engine, with the basic shape remarkably able to accommodate the increased size and power of the ever larger Merlins. Combining speed, firepower, and agility into one potent package, the Spit with it's distinctive elliptical wings is always remembered as the plane that won the Battle of Britain... though the Hawker Hurricane carried most of the load.
 
The Spitfire Mk1a was in service at the outbreak of WW2. The Spit V became available in March 1941, and the Spit IX, the most produced variant at 5,665 planes, was introduced in mid 1942.
 
James "Johnny" Johnson was the highest scoring British Spitfire pilot with 38 victories.