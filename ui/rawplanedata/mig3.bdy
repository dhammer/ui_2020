MIKOYAN-GUREVICH MiG-3
-= Full weight =-
7397 lb (3355 kg)

-= Best speed and FTH =-
327 mph at sea level (526 kmh)
355 mph at 8000 ft (571 kmh at 2400 m)
389 mph at 23000 ft (626 kmh at 7000 m)

-= Ca time to 16400 ft (5000 m) at Bst1 =-
6.4 min

-= Service Ceiling =-
38000 ft (11600 m)

-= Best turn rate at S.L., 50% fuel and no flaps =-
16.3 sec/lap, 632 ft radius

-= Power ON stall speed at 75% fuel =-
91 mph (146 kmh)

-= Max recommended dive speed (IAS) =-
404 mph (650 kmh)

-= Other notes =-
Sudden stall. 


The Mikoyan-Gurevich MiG-3 was a Soviet interceptor and fighter aircraft used during World War II. It was a development of the MiG-1 by the OKO (opytno-konstruktorskij otdel - Experimental Design Department) of Zavod (Factory) No. 1 to remedy problems that had been found during the MiG-1's development and operations. It replaced the MiG-1 on the production line at Factory No. 1 on 20 December 1940 and was built in large numbers during 1941 before Factory No. 1 was converted to build the Ilyushin Il-2.

On 22 June 1941 at the beginning of Operation Barbarossa, some 981 were in service with the VVS (the Soviet Air Force), the PVO (Soviet territorial air defense organization) and Naval Aviation. The MiG-3 was difficult to fly in peacetime and much more so in combat. It had been designed for high-altitude combat but combat over the Eastern Front was generally at lower altitudes where it was inferior to the German Messerschmitt Bf 109 as well as most modern Soviet fighters. It was also pressed into service as a fighter-bomber during the autumn of 1941 but it was equally unsuited for this. Over time the survivors were concentrated in the PVO, where its disadvantages mattered less, the last being withdrawn from service before the end of the war. 

In a combat environment where the MiG-3 was allowed to fight at high altitude though it could perform quite decently and had an impressive service ceiling for it's era.