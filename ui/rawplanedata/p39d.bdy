BELL P-39D "AIRACOBRA"
-= Full weight =-
7847 lb (3559 kg)

-= Best speed and FTH =-
348 mph at sea level (560 kmh)
368 mph at 5000 ft (592 kmh at 1500 m)
358 mph at 20000 ft (576 kmh at 6100 m)

-= Ca time to 16400 ft (5000 m) at Bst1 =-
6.8 min

-= Service Ceiling =-
32000 ft (9800 m)

-= Best turn rate at S.L., 50% fuel and no flaps =-
15.2 sec/lap, 564 ft radius

-= Power ON stall speed at 75% fuel =-
85 mph (137 kmh)

-= Max recommended dive speed (IAS) =-
523 mph (842 kmh)

-= Other notes =-
High top speed for it's era. Engine located in the rear fuselage. Powerful but slow firing cannon. Poor high altitude performance


The Bell P-39 Airacobra was not very popular with American and British pilots, who quickly dubbed it the "Iron Dog" due to its poor power to weight ratio. The P-39 and P-40 were all that was available to USAAC pilots early in the war however, and they served well in limited roles.
 
The XP-39 was introduced to the military in January 1940. The first production model, the P-39C, saw only limited numbers and was quickly superseded by the P-39D, which was the first to enter squadron service in February 1941. Early prototypes used a turbo-supercharged engine, but this was deemed unsuitable and removed, resulting in poor high altitude performance.
 
What the P-39 lacked in engine power, it made up in the sheer firepower of its armament. A spinner mounted 37mm cannon along with four .30 cal and two .50 cal machine guns meant that what the P-39 could hit, it killed. Sent under the Lend Lease act to Russia, pilots there found its cannon very effective against ground targets.
 
The leading P-39 ace was Aleksander Pokryshkin of the Red Air Force. His 59 victories, most of which came while flying the P-39, made him the second leading Allied ace of WW2.