On December 29th, 1939, the prototype of  the Liberator Bomber, XB24  was rolled out and flown for the first time. The chief feature that separated the B-24 from the B-17, was its use of a "Davis" wing. Early flight testing proved the Davis wing superior to its contemporaries.

Often overshadowed by the B-17, the Liberator achieved undeniable results during the Second World War. The B-24 was produced in far greater quantities than any other American aircraft before or since, with some 18,479 built. Although designed for heavy bombing, the B-24 was often used in other roles, such as reconnaissance, anti-ship / sub, rescue, transport, as well as a tanker, making it one of the most versatile aircraft of the war. Despite its inferior image, the B-24 was superior to the B17 in many ways, carrying a much heavier bomb load, further and faster. The B-24 was also an "all weather" aircraft, which the B-17 was not.  

In WarBirds, the B-24 may now be the players choice, due to increase bomb loads. Players need also be aware that many parts of the B-24 will not take as much damage as a B-17, in particular the wing. Defensive armament is comparable to the B-17, with an additional front turret in the B-2 J model.

Armament:                                           
Primary:  
B-24D - 9 x .50 Cal MG's 
B-24J - 10 X .50 Cal MG's 
Secondary: None       
All guns are gunner operated.
Gun positions are: Top, Bottom, Nose, Tail, Right (waist), and Left (waist).

Bomb Load
A normal bomb load for high-altitude missions was 5,000 pounds (2,250 kg), though it could accommodate an additional 3,000 pounds (1,350 kg) in the bomb bay and 8,000 pounds (3,600 kg) on external racks beneath the wings for short-range missions.