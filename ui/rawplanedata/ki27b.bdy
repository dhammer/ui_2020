NAKAJIMA KI-27B (NATE)
-= Full weight =-
3523 lb (1598 kg)

-= Best speed and FTH =-
255 mph at sea level (410 kmh)
292 mph at 11500 ft (470 kmh at 3500 m)
285 mph at 20000 ft (459 kmh at 6100 m)

-= Ca time to 16400 ft (5000 m) at Bst1 =-
6.1 min

-= Service Ceiling =-
32000 ft (9800 m)

-= Best turn rate at S.L., 50% fuel and no flaps =-
8.3 sec/lap, 277 ft radius

-= Power ON stall speed at 75% fuel =-
62 mph (100 kmh)

-= Max recommended dive speed (IAS) =-
400 mph (644 kmh)

-= Other notes =-
Fastest turning ww2 fighter in Warbirds. Relatively fragile. No pilot armor.


The Nakajima "Nate" Ki-27 was the first monoplane of the Japanese Army. It was a rather exceptional aircraft, because maneuverability had become a fetish for the Japanese air forces. The Ki-27 was certainly the most agile fighter monoplane ever built. It was a clean, very light, elegant monoplane with fixed, spatted landing gear and good performance; it marked the new ability of the Japanese industry to design and build advanced aircraft. Combat experience against Soviet fighters in the 'Nomonhan Incident' was not entirely favourable, because of the type's insufficient speed, armour and armament. It was still the most numerous Army fighter in December 1941. 3387 built.