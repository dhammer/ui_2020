T-6A TEXAN II
  
Mission 
The T-6A Texan II is a single-engine, two-seat primary trainer designed to train Joint Primary Pilot Training, or JPPT, students in basic flying skills common to U.S. Air Force and Navy pilots. 

Features 
Produced by Raytheon Aircraft, the T-6A Texan II is a military trainer version of Raytheon's Beech/Pilatus PC-9 Mk II. 

Stepped-tandem seating in the single cockpit places one crewmember in front of the other, with the student and instructor positions being interchangeable. A pilot may also fly the aircraft alone from the front seat. Pilots enter the T-6A cockpit through a side-opening, one-piece canopy that has demonstrated resistance to bird strikes at speeds up to 270 knots. 

The T-6A has a Pratt & Whitney Canada PT6A-68 turbo-prop engine that delivers 1,100 horsepower. Because of its excellent thrust-to-weight ratio, the aircraft can perform an initial climb of 3,100 feet (944.8 meters) per minute and can reach 18,000 feet (5,486.4 meters) in less than six minutes. 

The aircraft is fully aerobatic and features a pressurized cockpit with an anti-G system, ejection seat and an advanced avionics package with sunlight-readable liquid crystal displays.  
