During World War II, the armed forces of many countries used the C-47 and modified DC-3s for the transport of troops, cargo and wounded. Over 10,000 aircraft were produced in Long Beach and Santa Monica, California and Oklahoma City, Oklahoma. 

The C-47 was vital to the success of many Allied campaigns, in particular those at Guadalcanal and in the jungles of New Guinea and Burma where the C-47 (and its naval version, the R4D) alone made it possible for Allied troops to counter the mobility of the light-travelling Japanese army. 

Additionally, C-47s were used to airlift supplies to the embattled American forces during the Battle of Bastogne. But possibly its most influential role in military aviation was flying The Hump from India into China where the expertise gain would later be used in the Berlin Airlift in which the C-47 would also play its part.
