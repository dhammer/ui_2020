MESSERSCHMITT BF 110G-2
-= Full weight =-
17330.5 lb (7861 kg)

-= Best speed and FTH =-
316 mph at sea level (509 kmh) [1 min limit]
342 mph at 7000 ft (550 kmh at 2100 m) [1 min limit]
372 mph at 23000 ft (599 kmh at 7000 m) [1 min limit]

-= Ca time to 16400 ft (5000 m) at Bst1 =-
6.8 min

-= Service Ceiling =-
36000 ft (11000 m)

-= Best turn rate at S.L., 50% fuel and no flaps =-
16.8 sec/lap, 604 ft radius

-= Power ON stall speed at 75% fuel =-
86 mph (138 kmh)

-= Max recommended dive speed (IAS) =-
435 mph (700 kmh)

-= Other notes =-
Not very agile but packs a very heavy punch.


The Bf 110G-2 packs a very heavy punch with two 30 mm cannons and two 20 mm cannons, and can be even better armed with two extra 20 mm cannons in a gunpod, air-to-air rockets or even a devastating 3.7 cm BK cannon. The engines are of the DB 605B model, with the same power output as the DB 605A engine used by the Bf 109G-6. The 110G-2 has a somewhat upgraded defensive armament compared to the 110C-4, with a double barreled 7.92 mm MG, the MG 81Z.

The Messerschmit Bf-110 was an attempt at compromise. Conceived in 1934 and first flown as a prototype in 1936, the Bf-110 was intended as a strategic long range heavy fighter, or "Zerstorer".
 
The first production model, the Bf-110B, entered service in 1938. The Bf-110C joined the Luftwaffe in time for the invasion of Poland in 1939 and served as both a fighter and fighter bomber with great success as Germany drove across Europe. But in the Battle of Britain, when acting as bombers escorts, the pilots of the Bf-110 found they could not compete with the single engined Spitfires and Hurricanes of the RAF. Too heavy and too slow, it could not outrun the British fighters, nor could it outturn them. Relegated to ground attack and night fighter roles, it served admirably, and was the main German night fighter, equipped with radar to intercept nighttime bomber raids.
 
The Bf-110's reputation never recovered from the mauling it received over England, but it remained a versatile and effective weapon. The most successful Bf-110 pilot was Major Heinz-Wolfgang Schnaufer, who received the Diamonds to the Knight's Cross while claiming 121 kills as a night fighter.