Designed in 1937, the Mitsubishi A6M "Zero" or "Zeke" fighter 
was revolutionary and unbeatable to its Chinese opponents when 
it was introduced in the summer of 1940. 15 pre-production test 
models all but ended all Chinese attempts to intercept Japanese 
bombers over mainland China. Designed as a carrier borne fight-
er from the start, the A6M was the best in the world during the 
first years of WW2. 

Had the Japanese won the early victory in the Pacific as they 
planned, the Zero may have been enough, but the war dragged on, 
and the Zero was soon outclassed by better and still better 
generations of enemy fighters which Japan could not match.
 
Production of the A6M series totaled approximately 10,449. The 
A6M2 was the model that first saw action in China, the A6M3 was 
introduced in July 1941, and the A6M5a saw service starting in 
Autumn, 1943.
 
Japanese pilots saw the plane as the modern successor to the 
Samurai's sword, and offensive capability was the main consid-
eration. The Zero's turning ability was unmatched by any modern 
fighter, and its range was no less than astounding. US planners 
frequently mistook far-ranging land based Zekes for signs of 
the presence of a Japanese carrier close by. Two 20mm cannon 
and two 7.7mm MG's gave it respectable firepower. But the Zero 
had its shortcomings. 

The only front line naval fighter Japan 
used during most of the war, its speed and climbing ability 
were no match for more modern fighters. Its light construction 
left it vulnerable to damage that would hardly affect the heav-
ier opponents it faced, and even against the F4F, a plane it 
outperformed, this difference allowed superior team tactics by 
US fliers to defeat it. By the end of the war, Zero's were be-
ing used by Kamikaze pilots in the final defense of the home 
islands.
 
The top scoring Zero ace was Hiroyoshi Nishizawa. Nicknamed 
"The Devil" by his squadmates, he was credited with 113 vict-
ories although nobody knows an exact count for certain. Some 
figures place it lower, and some higher. His life came to an 
end when the transport plane he was flying in as a passenger 
was intercepted and shot down by American fighters.
 
In WarBirds, the Zeke is the furballer supreme. Only a Ki-43 
can beat it turn for turn. However, it still suffers from the 
inability to sustain damage, and many Zeros die to quick snap-
shots. Veteran opponents will generally use vertical manuever-
ing and speed to defeat the Zero, and in scenarios especially, 
its shortcomings come into play. Still, there's no quicker, 
easier way to have fun in the game than driving a few zekes in-
to a furball and wreaking havoc on low and slow enemy planes. 
The A6M5 in particular is a tough opponent, as it can climb 
with many of the US birds for a short time, making a "zoom" a 
bit dangerous.  
   
Armament:                     
                                   Ammo Load          
 
Primary  : 2 x 7.7mm MG's          680 rpg             
 
Secondary: 2 x 20mm cannon         100 rpg           
 
MG's are fuselage mounted. Cannon are wing mounted.
 
The "Ammo Load" is based on the historic normal operational 
ammo load for the plane/weapon(s) in question. 
 
 

