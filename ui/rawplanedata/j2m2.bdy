MITSUBISHI J2M2 "RAIDEN" (JACK)
-= Full weight =-
7329 lb (3324 kg)

-= Best speed and FTH =-
350 mph at sea level (563 kmh) [1 min limit]
379 mph at 6000 ft (610 kmh at 1800 m) [1 min limit]
402 mph at 20000 ft (645 kmh at 6100 m)

-= Ca time to 16400 ft (5000 m) at Bst1 =-
4.4 min

-= Service Ceiling =-
39000 ft (11900 m)

-= Best turn rate at S.L., 50% fuel and no flaps =-
14.3 sec/lap, 591 ft radius

-= Power ON stall speed at 75% fuel =-
90 mph (145 kmh)

-= Max recommended dive speed (IAS) =-
493 mph (793 kmh)

-= Other notes =-
Ailerons become heavy at high speeds. WEP can only be used for 1 min stints. 15 minutes water injection capacity per sortie.


Powered by the Mitsubishi MK4R-A Kasei 23a engine, the Raiden has an impressive max output of 2000 hp at 6000 ft. This WEP output is however only available for 1 minute before the engine temperature gets to high, and at military power the engine will produce 1740 hp at 9200 ft. The J2M2 is decently armed by 2x 7.7 mm MG's in the nose, and 2x 20 mm cannons in the wings. It also has pilot back armor and windscreen armor, allowing it's pilot to engage bombers without having to worry about a single machinegun bullet hitting the cockpit.

The Mitsubishi J2M2 entered combat in June 1944. It was a new more advanced fighter, designed by Jiro Horikoshi, the same man that earlier had designed the Mitsubishi A6M Zero. It was a formidable opponent when it arrived, but the Japanese army pilots were often fresh and lacked proper training, and the J2M Kasei 23a engine proved very difficult to maintain for the ground crews. The Japanese pilots were also outnumbered by more than 1:4 numbers at the time the J2M arrived and as potent as this fighter was, it had all the odds against it from the get go. That being said a fully functional J2M Raiden with a good pilot behind the stick is definitely a very competitive fighter, and fairly similar in performance to the Ki-84 and the N1K1. While American late war fighters will out run it, the Raiden easily out turns and out climbs them. While it is a lightweight late war fighter, the stall speed is fairly high. The controls also become heavy with increasing airspeed. Thanks to it's raw power/weight ratio though it is a great turner in a low n slow dogfight. The armament is also quite good and it can pack a punch. The J2M pilot really needs to save the WEP for crucial moments though, since the engine problems it suffered means the J2M will quickly overheat if run at to high manifold pressures. Most late war opponents will want to apply boom n zoom tactics against the J2M, while the Raiden pilot usually wants to make it a turn fight, depending on the opponent.