The TBF/TBM Avenger was the successor to the slow and short 
ranged TBD-1 Devastator.  In November of 1941, the prototype 
XTBF-1 reached a top speed of 271 mph with a range of 1,215 
miles without extra fuel, a significant improvement over the 
TBD.
 
The first production TBF-1 rolled off Grumman's assembly line 
in January 1942 and a second line at Eastern Aircraft began 
production of the TBM-1 in March 1943. The two aircraft types 
were virtually identical and could only be distinguished by 
comparing Bureau Numbers. A total of 2,293 TBF's were built by 
Grumman, and a total of 7,546 TBM's were built by Eastern.
 
The Avenger carried a pilot, radioman, and gunner, and was 
armed with two .50 MG in the wings for the pilot, one .50 MG in 
a rear dorsal turret for the gunner, and one .30 MG rear 
ventral "stinger" for the radioman. It had an internal bomb bay 
capable of carrying one 22 in. torpedo or 2000 lb of bombs. 
Some versions also carried wing rocket racks.
 
The Avenger is another addition to the WarBirds Pacific war 
scenario stable...and of course will also see use in the arenas 
when a CV group needs attention.
     
Armament:                     
                                Ammo Load    
 
Primary  : 2 x .50 cal MG's     600 rpg      
 
Secondary: None
 
Gunners: Upper ball: 1 x .50 cal MG        
         Lower tail: 1 x .30 cal MG
 
 
The "Ammo Load" is based on the historic normal operational 
ammo load for the plane/weapon(s) in question. 
 
 
&
