The M3 Scout Car (known as the White Scout Car in British Commonwealth service) was an American-produced armored car. The original M3 Scout Car was produced in limited numbers, while the improved M3A1 Scout Car saw wide service during World War II and after. 

In practice, the M3 Scout Car proved a serviceable military scout car. However, limitations were obvious as the vehicle lacked much in the way of protection for its crew. The M3 was not a direct-contact vehicle and avoiding combat with anything more than enemy infantry increased crew survivability. 

The base armor protection was only viable against small arms fire, offering basic security in an ambush. 

Armament:                                           
1 x 0.50 Browning M2 heavy machine gun
1 x 0.30 Browning M1919A4 machine gun

Ammunition load:

None
