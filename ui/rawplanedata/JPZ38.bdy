The Jagdpanzer 38 (Sd.Kfz. 138/2), was a German light tank destroyer of the Second World War based on a modified Czechoslovakian Panzer 38(t) chassis.

German armored forces in World War II created a variety of vehicles by mounting anti-tank guns on obsolete tank chassis. These machines performed even better than expected, yet were still vulnerable due to high vehicle profiles and open-topped turrets. Allied bombings took a heavy toll on German production facilities, and further increased the need for an easily produced yet effective light tank destroyer to replace vehicles like the StuG III and Marder series.

Jagdpanzer 38s first entered service in July 1944, and would eventually be assigned to a number of units, including infantry, Panzerjäger and Volksgrenadier divisions. BMM and Škoda continually modified and improved the Jagdpanzer 38 during production of the more than 2,800 vehicles built. Owing to the ease of production and high operating rates, the Jagdpanzer 38 came to serve as Germany's main tank destroyer in the latter period of the war, making an important contribution on both the Eastern and Western Fronts.

Armament:                                           

The primary armament was one 7.5cm PaK 39 (L/48) gun located some 0.38m (15in) to the right of the hull centerline, and fitted with an improved type of recoil mechanism permitting the muzzle brake to be omitted. 

The PaK 39’s elevation arc was -6° to +10° and its traverse arc was 11° right and 5° left of the centerline, and 41 rounds of ammunition were carried. One 7.92mm machine gun was supported on a mounting built into the roof and fitted with a periscopic sight and extended trigger mechanisms
