FOCKE WULF FW 190A-8
-= Full weight =-
9376.7 lb (4253 kg)

-= Best speed and FTH =-
366 mph at sea level (589 kmh)
383 mph at 4000 ft (616 kmh at 1200 m)
408 mph at 21000 ft (657 kmh at 6400 m)

-= Ca time to 16400 ft (5000 m) at Bst1 =-
6.6 min

-= Service Ceiling =-
36000 ft (11000 m)

-= Best turn rate at S.L., 50% fuel and no flaps =-
19.0 sec/lap, 829 ft radius

-= Power ON stall speed at 75% fuel =-
101 mph (163 kmh)

-= Max recommended dive speed (IAS) =-
466 mph (750 kmh)

-= Other notes =-
Very thick engine armor, engine can take more hits than normal. Superb roll rate. Quite sudden stall. 20 minutes methanol-water injection capacity per sortie.


The Fw 190A-8 was a further step in development compared to the A-4. The A-8 had much thicker engine armor, allowing it to attack bombers and ground targets with less risk of engine damage. It could also be configured as a "Sturmbock" fighter, carrying 2x 30 mm cannons. A further improvement was the engine's ability to use a methanol/water 50/50 injection (MW 50), significantly increasing the max power output of the engine while also being able to run at max power for a longer time.

After a short association with the Fw-190, many new players imagine this aircraft as a large monstrous brute of a plane. In reality, the Wuerger (Butcherbird) is a compact, elegant, and beautiful fighter.
 
Conceived by Focke Wulf designer Kurt Tank in 1937, the Fw-190 only saw production because it used a BMW air cooled radial engine rather than the inline liquid cooled engines earmarked for the Bf-109. When it was introduced, it was more than a match for the RAF fighters it faced, and it maintained a fearsome reputation throughtout the war. The Focke Wulf fighter saw many variants, and served with fighter/interceptor, dive bombing, recon, and even torpedo groups everywhere the Luftwaffe fought. 
 
The first Fw-190 prototype flew in June 1939 and production versions started rolling off the assembly line in November 1940. Of the versions modeled in the game, The Fw-190A-4 entered service in the Summer of 1942 and gave allied pilots a powerful opponent. The heavily gunned A-8 variant first saw action in Mid 1944 and was produced in the largest numbers of any version, and the Fw-190D-9, a high altitude fast interceptor with the Jumo inline liquid cooled engine, was made only in small numbers late in the war though it is considered the finest prop driven fighter the Germans produced in WW2.
 
Erich Rudorffer scored a record 13 kills in one sortie while flying the Fw-190, a testament to the lethality of the plane.
 
The thing that most gives the Butcherbird its awesome reputation is its armament. Most versions featured 4 20mm cannon along with two 7.9mm machine guns. The Fw-190A-8 is the heaviest armed fighter in the game with two of its four 20mm cannon being high velocity MG-151's, and two 13mm MG's in the cowling.