KAWANISHI N1K1-J "SHIDEN" (GEORGE)
-= Full weight =-
8598 lb (3900 kg)

-= Best speed and FTH =-
359 mph at sea level (578 kmh) [1 min limit]
371 mph at 2500 ft (597 kmh at 800 m) [1 min limit]
406 mph at 20000 ft (653 kmh at 6100 m)

-= Ca time to 16400 ft (5000 m) at Bst1 =-
4.8 min

-= Service Ceiling =-
38000 ft (11600 m)

-= Best turn rate at S.L., 50% fuel and no flaps =-
14.5 sec/lap, 599 ft radius

-= Power ON stall speed at 75% fuel =-
89 mph (143 kmh)

-= Max recommended dive speed (IAS) =-
495 mph (797 kmh)

-= Other notes =-
Elevator very effective at high speeds, be careful of over pulling G's. Quite agile. WEP can only be used for 1 min stints. 15 minutes water injection capacity per sortie.


The Kawanishi N1K1-J Shiden was an Imperial Japanese Navy Air Service land-based version of the N1K. Assigned the Allied codename "George", the N1K1-J was considered by both its pilots and opponents to be one of the finest land-based fighters flown by the Japanese during World War II. Despite such capability, it was produced too late and in insufficient numbers to affect the outcome of the war.

The "Shiden" was similar in performance to the J2M and Ki-84. It was heavier but could hold a turn just as good and the controls were effective at all speeds. According to Allied testpilots the elevator was almost dangerously responsive at higher speeds.

The N1K1 entered service in early 1944 and proved highly effective against American fighters. The Kawanishi was among the few Japanese fighters that could stand up to the best enemy types, including Hellcats and Corsairs. In the hands of aces, the Shiden could even outfly it's American opponents. In February 1945, Lieutenant Kaneyoshi Muto, flying a N1K2-J as part of a group of at least 10 expert Japanese pilots, faced seven U.S. Navy Hellcats of VF-82 in the sky over Japan. His group shot down four Hellcats with no loss to themselves. After the action, reporters fabricated a story in which Muto was the sole airman facing 12 enemy aircraft. However, a close friend of Lieutenant Kaneyoshi Muto, ace pilot Saburo Sakai, states in his autobiography that the one versus twelve combat did take place, but with Muto at the controls of a Zero fighter. 

The N1K1-J aircraft were used very effectively over Formosa, the Philippines and later, Okinawa. Before production was switched to the improved N1K2-J, 1,007 aircraft were produced, including prototypes.
