YAKOVLEV YAK-3
-= Full weight =-
5945.9 lb (2697 kg)

-= Best speed and FTH =-
354 mph at sea level (570 kmh)
358 mph at 1000 ft (576 kmh at 300 m)
384 mph at 13000 ft (618 kmh at 4000 m)

-= Ca time to 16400 ft (5000 m) at Bst1 =-
4.6 min

-= Service Ceiling =-
35000 ft (10700 m)

-= Best turn rate at S.L., 50% fuel and no flaps =-
15.3 sec/lap, 682 ft radius

-= Power ON stall speed at 75% fuel =-
94 mph (151 kmh)

-= Max recommended dive speed (IAS) =-
435 mph (700 kmh)

-= Other notes =-
Great roll rate. High wingloading but light and agile. Limited ammunition.


While the Yak-3 and Yak-9D might look similar, the Yak-3 was a significantly smaller aircraft both in shape, wing area and weight compared to the Yak-9D. The Yak-3 was more agile, faster and climbed better while the Yak-9D had more fuel for long range missions. They both used the Klimov VK-105PF engine but the Yak-3 used a version of it which allowed for a better max boost. The 1945 Yakovlev version, the Yak-9U, was based on the heavier Yak-9D but used a much improved Klimov VK-107a engine, producing a maximum of 1650 hp compared to the 1300 hp of the Yak-3. The Yak-9U was considered the best in the series, and while being heavier and slightly less agile than the Yak-3, the stronger powerplant of the Yak-9U gave it a better top speed and power/weight ratio.

The Yakelov Yak-3 was a development of the Yak-1, a fighter which had seen service since 1940. Where the Yak-1 was outclassed by the Luftwaffe fighters it faced, the Yak-3, flying at low altitudes (where most combat took place over the large tank and infantry battles raging on the Eastern front), was clearly able to match it's German rivals.

It was one of the lightest, most maneuverable fighter to see action in the European theatre, born of the Soviet's need for an easily and cheaply produced fighter which did not require the hard-to-get materials used in other designs. The reliable VK-105PF2 engine gave it good speed and climb. It's armament, consisting of two cowl mounted 12.7mm MG's and a single 20mm cannon mounted through the engine, was light by the standards of the day but still adequate.

While many sources state that the Yak-3 made its debut on the frontlines over the battle of Kursk in July 1943, this was probably a cut-down version of the Yak-1. The first Yak-3 series plane flew in March 1944 and entered service in the 2nd quarter of that year. In any case, Soviet pilots were enthusiastic with the new fighter, as it finally put them in an equal or better mount than their opponents. 

Yakelov factories produced 4,797 Yak-3's of all variants through the end of the war. 

Soviet Maj. S D Lugansky scored 43 kills in the Yak-3, making him probably the leading ace of the type.