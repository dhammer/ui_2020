MESSERSCHMITT BF 109F-1 "FRIEDRICH"
-= Full weight =-
6014.2 lb (2728 kg)

-= Best speed and FTH =-
321 mph at sea level (517 kmh)
351 mph at 7000 ft (565 kmh at 2100 m)
375 mph at 16000 ft (604 kmh at 4900 m)

-= Ca time to 16400 ft (5000 m) at Bst1 =-
5.3 min

-= Service Ceiling =-
39000 ft (11900 m)

-= Best turn rate at S.L., 50% fuel and no flaps =-
13.7 sec/lap, 470 ft radius

-= Power ON stall speed at 75% fuel =-
78 mph (126 kmh)

-= Max recommended dive speed (IAS) =-
470 mph (756 kmh)

-= Other notes =-
Good roll rate. Elevator quickly becomes heavy with increasing speed.


The F's might be the pinnacle of the Messerchmitt series and the wing cannons have been removed for a single nose cannon. The Bf 109F's and future 109's have redesigned wings with a better wing efficiency and lower drag than the E's, and can also bring a droptank. The F-1 benefits from a good power/weight ratio with the DB 601N engine capable of producing 1243 hp at 6900 ft for 5 minute stints.

The Bf-109 in all its variants was produced in larger numbers than any other fighter in history, with over 30,000 built. By itself, that number speaks of the success of this aircraft. Also known as the Me-109 after its creator, Dr. Willy Messerschmitt, the first Bf-109 prototype took flight in 1935 and the final service variant was phased out by the Spanish Air Force in 1967, a career of over 30 years. 
 
Of the types modeled in WarBirds, the 109E began appearing in 1939 and participated in the Battle of Britain. The Bf-109F4 entered service early in 1942 and introduced more drag reduction to the design as well as moving the wing MG's to the nose. The G6 model appeared in December 1943 with the G6/R6 a more heavily armed subvariant of that model, and the Bf-109K4, a high speed high altitude interceptor version, began arriving in October 1944.
 
The Bf-109 incorporated several new advances in structural design and aerodynamics when it was built. 5 years after its first introduction, it remained superior to any opposing fighter, and its many variations kept it abreast of the likes of the Spitfire MkIX and other allied planes throughout the war. However, the 109's success was bolstered in part by the large corps of extremely experienced pilots flying the plane. 
 
The leading fighter ace of all time, Oberst Erich Hartmann, flew the Bf-109 for all of his 352 aerial victories, and there were literally dozens of others with well over 100 kills in 109 variants.