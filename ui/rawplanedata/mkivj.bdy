PzKpfw IV ausf J

Introduced in march of 1944 the ausf J was the final production model of the PzKpfw IV. Very similar to the ausf H, the J featured a simplified structure with various details being eliminated to simplify construction. 

This version incorporating the final changes in armor improvement as well as gun and ammunition improvement. ON some versions hull skirts and turret skirts being used to up the armor protection for hollow charged weapons and to break up AP shot before making contact with the main body of the tank. 

Some version even incorporating the use of "Thoma shields" made of heavy mesh wire for anti bazooka protection. This version was produced right up until the last month of the war and served after WW 2 in several countries, lasting in active service long enough to take part in the 1967 Arab-Israeli war, used by Syrian armored forces primarily as dug in anti-tank weapons.
