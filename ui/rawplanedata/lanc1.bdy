4 x RR Merlin XX Engines 

Full throttle height: 17,000ft  

Maximum take-off weight: 70,000 lbs 

Service Ceiling : Circa 20,000ft 

Recommended Climb Speed :  160mph IAS
 
Trims for take-off: 
  Rudder  -15   
  Elevator -4 

Flaps for take-off: 15-20 degrees  


The Avro Lancaster was the best Royal Air Force bomber of the war. Weighing in at 36,900 empty weight pounds, the Lanc could carry another 33,100 pounds of fuel and ordinance. It hauled most of the bombs dropped by the RAF and RCAF during World War II, but was most famous for it's special operations, specifically the "Dambusters" raid in which a specially designed bomb was dropped in such a way as to explode below the water line abutting the dam, forcing the water's shockwave to do the dirty work of destroying the damn. Something a direct hit couldn't have accomplished on it's own.

Crew comfort was a low priority in this very utilitarian bomber. It flew in darkness for a majority of it's missions and had little to no defensive armor, only carrying three hydraulically driven .303 machine gun turrets with a total of 8 guns between them. These gunners stayed in their cramped quarters for the whole flight, at night, in terribly cold conditions.

The bomb bay stretched out 33 feet and had no dividers, making for a versatile load-out inventory.
     
Armament:                     
                                   Ammo Load          
 
Gunners  : 3 x .303 cal MG's       Varied                   
 
All guns are gunner operated. Gun positions are: Top, Nose, and Tail.