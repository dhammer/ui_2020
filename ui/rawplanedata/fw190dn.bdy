After a short association with the Fw-190, many new players imagine this aircraft as a large monstrous brute of a plane. In reality, the Wuerger (Butcherbird) is a compact, elegant, and beautiful fighter.
 
Conceived by Focke Wulf designer Kurt Tank in 1937, the Fw-190 only saw production because it used a BMW air cooled radial engine rather than the inline liquid cooled engines earmarked for the Bf-109. When it was introduced, it was more than a match for the RAF fighters it faced, and it maintained a fearsome reputation throughtout the war. The Focke Wulf fighter saw many variants, and served with fighter/interceptor, divebombing, recon, and even torpedo groups everywhere the Luftwaffe fought. 
 
The first Fw-190 prototype flew in June 1939 and production versions started rolling off the assembly line in November 1940. Of the versions modeled in the game, The Fw-190A-4 entered service in the Summer of 1942 and gave allied pilots a powerful opponent. The heavily gunned A-8 variant first saw action in Mid 1944 and was produced in the largest numbers of any version, and the Fw-190D-9, a high altitude fast interceptor with the Jumo inline liquid cooled engine, was made only in small numbers late in the war though it is considered the finest prop driven fighter the Germans produced in WW2.
 
Erich Rudorffer scored a record 13 kills in one sortie while flying the Fw-190, a testament to the lethality of the plane.
 
The thing that most gives the Butcherbird its awesome reputation is its armament. Most versions featured 4 20mm cannon along with two 7.9mm machine guns. The Fw-190A-8 is the heaviest armed fighter in the game with two of its four 20mm cannon being high velocity MG-151's, and two 13mm MG's in the cowling.
 
WarBirds Fw pilots soon learn the art of energy fighting or move on to other aircraft. Not quite the fastest plane in the game, running is often not an option, and not among the best turning planes in the game, tight furballing leads to death.What this plane excels at is diving on opponents, blowing them to fragments in one brutal firing pass, climbing back to its high perch, and seeking out its next victim. An expert can take this plane into a furball and survive using vertical maneuvers and its outstanding roll rate, racking up kills with the lethal guns, but one mistake leaves the Fw-190 defensive and soon defeated.
     
Armament:                     
                                   Ammo Load       
 
Primary  : 2 x 7.9mm MG's          900 rpg         
 
Secondary: 2 x 20mm MGFF cannon    60 rpg          
                                                   
           2 x 20mm MG151 cannon   200 rpg
 
MG's are fuselage mounted. The cannon are wing and wing-root mounted.

The "Ammo Load" is based on the historic normal operational ammo load for the plane/weapon(s) in question.