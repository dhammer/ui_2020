SUPERMARINE SPITFIRE LF Vc
-= Full weight =-
6612.6 lb (2999 kg)

-= Best speed and FTH =-
333 mph at sea level (536 kmh)
356 mph at 6000 ft (573 kmh at 1800 m)
326 mph at 25000 ft (525 kmh at 7600 m)

-= Ca time to 16400 ft (5000 m) at Bst1 =-
5.6 min

-= Service Ceiling =-
35000 ft (10700 m)

-= Best turn rate at S.L., 50% fuel and no flaps =-
11.5 sec/lap, 456 ft radius

-= Power ON stall speed at 75% fuel =-
78 mph (126 kmh)

-= Max recommended dive speed (IAS) =-
450 mph (724 kmh)

-= Other notes =-
Reduced negative G's engine cutout effect (Miss Shilling's orifice). Agile with a gentle stall behaviour.


The LF Vc has the Merlin 45M engine. The "LF" prefix indicates that it's engine is tooled for low altitude performance, and it's 10 mph faster at sea level than the Mk.Vb. Above 7000 ft though the Vb has a clearly superior performance, and the LF Vc is almost 30 mph slower at 15000 ft. A big advantage with the LF Vc is it's increased cannon ammo load, with 240 rounds compared to the 120 rounds of the Vb.

In addition to being one of the most beautiful fighters ever built, the Supermarine Spitfire was one of the most robust designs. It had a great wing efficiency due to the eliptical shape of it's wings, resulting in less induced drag effects. With the same airframe that first flew as a prototype in 1936, the Spitfire remained in production throughout WW2, the only Allied fighter to do so. Improvements were mainly to the engine, with the basic shape remarkably able to accommodate the increased size and power of the ever larger Merlins. Combining speed, firepower, and agility into one potent package, the Spit with it's distinctive elliptical wings is always remembered as the plane that won the Battle of Britain... though the Hawker Hurricane carried most of the load.
 
The Spitfire Mk1a was in service at the outbreak of WW2. The Spit V became available in March 1941, and the Spit IX, the most produced variant at 5,665 planes, was introduced in mid 1942.
 
James "Johnny" Johnson was the highest scoring British Spitfire pilot with 38 victories.