The major changes in the ausf G were the introduction of the double 
baffle muzzle brake on the kwk 40 with even slightly longer barrel for a 
further increase in muzzle velocity, and the elimination of the two 
turret forward vision ports. Late production ausf G�s were fitted with 
one piece cupola hatches and extra 30mm fitted plates added to the nose 
and hull front plate.
