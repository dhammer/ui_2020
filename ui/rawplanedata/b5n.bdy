When the Pacific War began the Japanese Navy had in the Nakajima B5N2 (code name: "Kate") the most advanced carrier borne torpedo bomber in the world. 144 Kates participated in the attack on Pearl Harbor and in the next 12 months B5N2's delivered fatal blows to three US Navy Carriers. Powered by a nine cylinder air cooled radial engine, the B5N1 prototype was flown in January 1937 and the plane went into production in November of that year. In 1939, after facing the Chinese in the Sino-Japanese War, the B5N was improved and the B5N2 with its 1400 hp 14 cylinder double row engine, though not appreciably faster despite the increased power, was considerably more reliable.
 
Although successful in the early stages of the war, the Kate was obsolete by 1944 and relegated to second line units. There they found use as recon aircraft and provided some cover to Japanese shipping against submarine attack. Some models were outfitted with early Air-to-Surface Radar for this role. A total of 1,149 B5N's were produced.
 
In WarBirds, the Kate is pressed into service in scenarios depicting the carrier battles of the Pacific, and sees use in the arena when that enemy CV just needs to be taken out NOW. Hitting a moving ship with its payload requires one of two things: An very practiced long range shooter of a pilot, or a suicidal close range release. The ack on the carrier fleets is murderous when a close range Kate goes into the required low and level flight required for torpedo release.  
   
Armament:                     
                                      Ammo Load       
 
Primary  : Tail Gunner 1 x 7.7mm MG   679 rpg                                    
 
Secondary: None       
 
No forward firing guns.
 
The "Ammo Load" is based on the historic normal operational ammo load for the plane/weapon(s) in question.