The M4 Sherman, officially Medium Tank, M4, was the most widely used medium tank by the United States and Western Allies in World War II. The M4 Sherman proved to be reliable, relatively cheap to produce, and available in great numbers. Thousands were distributed through the Lend-Lease program to the British Commonwealth and Soviet Union. The tank was named by the British for the American Civil War general William Tecumseh Sherman.

The M4 Sherman evolved from the M3 Medium Tank,[N 1] which had its main armament in a side sponsion mount. The M4 retained much of the previous mechanical design, but put the main 75 mm gun in a fully traversing turret. One feature, a one-axis gyrostabilizer, was not precise enough to allow firing when moving but did help keep the reticle on target, so that when the tank did stop to fire, the gun would be aimed in roughly the right direction.

Armament:                                           
Primary:  
1 x 75mm main gun
1 x .50 caliber Browning M2 turret-mounted anti-aircraft machine gun
1 x .30-06 caliber co-axial mounted machine gun
1 x .30-06 caliber bow-mounted machine gun.

Ammunition load:
97 x 75mm projectiles.
300 x .50 caliber ammunition.
4,750 x .30-06 caliber ammunition.

