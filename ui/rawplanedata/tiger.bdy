The PzKpfw VIAusf H, aka the Tiger, was one of the most feared weapons of the war. The Tiger tank was very heavily armoured and carried a massive 88mm cannon on board. Early engagements gave the Tiger an aura of invincibility. However, such a status was not necessarily deserved as the Tiger could be stopped and its sheer size caused problems.

The development of the Tiger began in 1939 and was accelerated after May 1941 when the Wehrmacht asked for a 45 ton tank which had as its principle weapon an 88mm gun. The 88mm gun had already proved itself in battle as an artillery weapon.

On April 20th, 1942, two versions from both Henschel and Porsche were displayed in front of Hitler at his base in Rastenburg. The Henschel design was found to be the superior tank. The full production of the first Tiger tank started in August 1942.

The Tiger was in production for two years, reaching 1350 total produced.

For all its formidable weaponry, the Tiger had its problems � and one of these was its tracks. During the winter, mud and snow would pack into the tracks and jam them. When the Russians realised this, they timed their attacks for the early morning before the snow/mud could thaw out.

The sheer size of the Tiger was also a problem. Few bridges were strong enough to cope with the ever increasing weight of the various different marks of the Tiger.

The Kursk offensive saw the first large scale use of the Tiger, which was slower than a medium MKIII or MKIV. It typically fought with the faster but less well-armed MKIII�s or MKIV�s protecting its flanks.

The Allies first met the Tiger at Tunisia.  French shells from a 75mm gun bounced off the hull � from a distance of just 50 metres. The tank was also successful elsewhere � but again, behind the success, lay some major weaknesses. A journey of just 60 miles by a Tiger could eat up 150 gallons of fuel. Maintaining a decent fuel supply to Tiger columns was always a difficult process and one that could be very easily disrupted by resistance fighters.

Within Normandy, the Tigers scored victories out of proportion to their numbers. On July 11th, 1944, thirteen British Shermans were lost of out 20 with two more captured with no Tiger losses. The Tigers did well enough to survive the onslaught at the Falaise Gap and in August just 2 Tigers held up the advance of the 53rd British Infantry division.