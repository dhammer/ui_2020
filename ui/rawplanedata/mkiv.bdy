Panzerkampfwagen IV Ausf H entered production in April of 1943 as the ninth variant of Panzerkampfwagen IV tank family (9 Serie BW). This medium tank received a designation of Sd.Kfz.161/2. Production took place until July of 1944 and Ausf H reached the largest production figures of any Panzerkampfwagen IV model with 3774 produced by Krupp-Gruson AG in Magdeburg, Vomag AG in Plauen and Nibelungenwerke AG in St.Valentin. 

The last company produced the most of Ausf H tanks until it switched production to the next and final variant Ausf J. Panzerkampfwagen IV Ausf H entered production in April of 1943 as the ninth variant of Panzerkampfwagen IV tank family (9 Serie BW). 

This medium tank received a designation of Sd.Kfz.161/2. Production took place until July of 1944 and Ausf H reached the largest production figures of any Panzerkampfwagen IV model with 3774 produced by Krupp-Gruson AG in Magdeburg, Vomag AG in Plauen and Nibelungenwerke AG in St.Valentin.

The last company produced the most of Ausf H tanks until it switched production to the next and final variant Ausf J.
